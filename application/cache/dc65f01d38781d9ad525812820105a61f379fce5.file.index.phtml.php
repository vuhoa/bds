<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-09-10 13:46:52
         compiled from "application\views\templates\home\index.phtml" */ ?>
<?php /*%%SmartyHeaderCode:15135041025d7746dc5782c5-05671697%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dc65f01d38781d9ad525812820105a61f379fce5' => 
    array (
      0 => 'application\\views\\templates\\home\\index.phtml',
      1 => 1555644121,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15135041025d7746dc5782c5-05671697',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'section1' => 0,
    'section2' => 0,
    'section3' => 0,
    'section4' => 0,
    'section5' => 0,
    'section6' => 0,
    'homeDocs' => 0,
    'homeImage' => 0,
    'homeVideo' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d7746dc764634_65569363',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d7746dc764634_65569363')) {function content_5d7746dc764634_65569363($_smarty_tpl) {?><section class="container" id="home-page">
    <div class="title-page"><h1><?php echo $_smarty_tpl->tpl_vars['section1']->value['name'];?>
</h1></div>
    <div class="box-nav"></div>
    <div class="box-slider">
        <!--LEFT CONTENT-->
        <div class="box-left">
            <!--INTRO-->
            <div class="group-left" data-name="intro-home">
                <div class="box-cover"></div>
                <div class="center-content">
                    <div class="logo-center"></div>
                    <span class="line"></span>
                    <h2 class="slogan"><?php echo $_smarty_tpl->tpl_vars['section1']->value['title'];?>
</h2>
                </div>
                <!--VIDEO-->
                <div class="box-video-center">
                    <div class="video-cover" id="videocontainer" data-fullscreen="false">
                        <a class="player-vid" id="playervid" href="javascript:void(0);"></a>
                        <div class="pic-video" style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section1']->value['img'];?>
)"></div>
                        <video id="video-full" class="video-full" preload="auto" loop="" muted="no" controls="play">
                            <source src="<?php echo $_smarty_tpl->tpl_vars['section1']->value['video'];?>
" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                        </video>
                        <!--VIDEO CONTROL-->
                        <div id="videocontrols" class="controls" data-state="hidden">
                            <button id="stop" type="button" data-state="stop"></button>
                            <button id="playpause" type="button" data-state="play"></button>
                            <div class="progress">
                                <progress id="progress" value="0" min="0"><span id="progressbar"></span></progress>
                            </div>
                            <button id="mute" type="button" data-state="play"></button>
                            <button id="fullscreen" type="button" data-state="go-fullscreen"></button>
                        </div>
                        <!--VIDEO CONTROL-->
                    </div>
                </div>
                <!--VIDEO-->
            </div>
            <!--INTRO-->
            <!--banner su kien-->
            <div class="group-left" data-name="su-kien">
                <div class="box-cover" style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section2']->value['img'];?>
)"></div>
                <div class="text-intro">
                    <a class="go-page buton-sk" href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['section3']->value['slug'];?>
.html">Đăng ký nhận thư
                        mời<span></span></a>
                </div>
            </div>
            <!--End banner su kien-->
            <!--LOCATION-->
            <div class="group-left" data-name="location-home">
                <div class="box-cover" style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section3']->value['image'];?>
)"><img
                            src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section3']->value['background'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['section3']->value['title'];?>
"></div>
                <div class="text-intro">
                    <h2><?php echo $_smarty_tpl->tpl_vars['section3']->value['title'];?>
</h2>
                    <div class="box-text">
                        <?php echo $_smarty_tpl->tpl_vars['section3']->value['more'];?>

                    </div>
                    <a class="go-page" href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['section3']->value['slug'];?>
.html"><?php echo $_smarty_tpl->tpl_vars['section3']->value['name'];?>
<span></span></a>
                </div>
            </div>
            <!--LOCATION-->
            <!--LOCATION 2---->
            <div class="group-left" data-name="apartment-home2">
                <div class="box-cover my-box-cover"
                     style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section4']->value['background'];?>
)"><a
                            href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['section4']->value['slug'];?>
.html"><img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section4']->value['image'];?>
"
                                                                         alt="<?php echo $_smarty_tpl->tpl_vars['section4']->value['title'];?>
"></a></div>
                <div class="text-intro my-apartment-home2">
                    <h2><?php echo $_smarty_tpl->tpl_vars['section4']->value['title'];?>
</h2>
                    <div class="box-text">
                        <?php echo $_smarty_tpl->tpl_vars['section4']->value['more'];?>

                    </div>
                    <a class="go-page" href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['section4']->value['slug'];?>
.html"><?php echo $_smarty_tpl->tpl_vars['section4']->value['name'];?>
<span></span></a>
                </div>
            </div>
            <!--LOCATION 2-->
            <!--FACILITIES-->
            <div class="group-left" data-name="facilities-home">
                <div class="box-cover"
                     style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section5']->value['background'];?>
)"><img
                            src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section5']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['section5']->value['title'];?>
"></div>
                <div class="text-intro">
                    <h2><?php echo $_smarty_tpl->tpl_vars['section5']->value['title'];?>
</h2>
                    <div class="box-text">
                        <?php echo $_smarty_tpl->tpl_vars['section5']->value['more'];?>

                    </div>
                    <a class="go-page" href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['section5']->value['slug'];?>
.html">Xem Chi Tiết<span></span></a>
                </div>
            </div>
            <!--FACILITIES-->
            <!--APARTMENT-->
            <div class="group-left" data-name="apartment-home">
                <div class="box-cover"
                     style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section6']->value['background'];?>
)"><img
                            src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section6']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['section6']->value['title'];?>
"></div>
                <div class="text-intro">
                    <h2> <?php echo $_smarty_tpl->tpl_vars['section6']->value['title'];?>
</h2>
                    <div class="box-text">
                        <?php echo $_smarty_tpl->tpl_vars['section6']->value['more'];?>

                    </div>
                    <a class="go-page" href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['section6']->value['slug'];?>
.html">Xem Thông Tin<span></span></a>
                </div>
            </div>
            <!--APARTMENT-->
            <!--NEWS-->
            <div class="group-left" data-name="news-home" id="section-album">
                <div class="box-cover"
                     style="background-image:url(<?php echo base_url();?>
files/background/bg8.jpg)"></div>
                <div class="wrap-news">
                    <div class="box album-home tlda">
                        <div class="colum-pic">
                            <img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['homeDocs']->value['img'];?>
"
                                 alt="<?php echo $_smarty_tpl->tpl_vars['homeDocs']->value['name'];?>
">
                        </div>
                        <a class="view-album" href="javascript:void(0);" data-href="<?php echo base_url('chi-tiet-tai-lieu');?>
.html"></a>
                        <h2><?php echo $_smarty_tpl->tpl_vars['homeDocs']->value['name'];?>
 </h2>
                    </div>
                    <div class="box album-home">
                        <div class="colum-pic">
                            <img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['homeImage']->value['img'];?>
"
                                 alt="<?php echo $_smarty_tpl->tpl_vars['homeImage']->value['name'];?>
">
                        </div>
                        <a class="view-album" href="javascript:void(0);"
                           data-href="<?php echo base_url('chi-tiet-can-ho');?>
.html"></a>
                        <h2><?php echo $_smarty_tpl->tpl_vars['homeImage']->value['name'];?>
</h2>
                    </div>
                    <div class="box video-home">
                        <div class="colum-pic">
                            <img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['homeVideo']->value['img'];?>
"
                                 alt="<?php echo $_smarty_tpl->tpl_vars['homeVideo']->value['name'];?>
">
                        </div>
                        <a class="player" href="javascript:void(0);"
                           data-href="<?php echo base_url('chi-tiet-video');?>
.html"></a>
                        <h2><?php echo $_smarty_tpl->tpl_vars['homeVideo']->value['title'];?>
</h2>
                    </div>
                </div>
            </div>
            <!--NEWS-->
            <!--CONTACT-->
            <div class="group-left" data-name="contact-home">
                <div class="box-cover"></div>
                <div class="investor mobile">
                    <ul>
                        <li><a class="logo-angia vi" href="#" target="_blank"></a></li>
                        <li><a class="logo-creed vi" href="javascript:void(0);" target="_blank"></a></li>
                    </ul>
                </div>
                <div class="text-intro">
                    <h2><?php echo $_smarty_tpl->tpl_vars['section2']->value['title'];?>
</h2>
                    <div class="box-text">
                        <?php echo $_smarty_tpl->tpl_vars['section2']->value['more'];?>

                        <form method="POST" action="<?php echo base_url('lien-he');?>
.html"
                              accept-charset="UTF-8" id="register">

                            <div class="require-col">
                                <div class="input-text">
                                    <input id="subscription_customer_attributes_full_name" type="text"
                                           placeholder="Họ tên Quý khách"
                                           class="quantumWizTextinputPaperinputInput exportInput"
                                           autocomplete="off" name="username" required />
                                </div>
                                <div class="input-text">
                                    <input id="subscription_customer_attributes_email" type="email" value=""
                                           placeholder="Email" class="quantumWizTextinputPaperinputInput exportInput"
                                           name="email" required />
                                </div>
                                <div class="input-text">
                                    <input id="subscription_customer_attributes_phone" type="tel" value=""
                                           placeholder="Điện thoại"
                                           class="quantumWizTextinputPaperinputInput exportInput" name="phone" required />
                                </div>
                                <div class="input-but">
                                    <button class="button" id="btn-register-submit" type="submit">Đăng ký nhận thông tin</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--CONTACT-->
        </div>
        <!--LEFT CONTENT-->

        <!--RIGHT CONTENT-->
        <div class="box-right">
            <!--INTRO-->
            <div class="group-right" data-name="intro-home">
                <div class="box-cover-right"></div>
            </div>
            <!--INTRO-->
            <!--Su kien-->
            <div class="group-right" data-name="su-kien">
                <div class="box-cover-right" style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section2']->value['image'];?>
)"></div>
            </div>
            <!--end su kien-->
            <!--LOCATION-->
            <div class="group-right" data-name="apartment-home2">
                <div class="box-cover-right" style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section3']->value['img'];?>
)"></div>
            </div>
            <!--LOCATION-->
            <!--LOCATION-->
            <div class="group-right" data-name="location-home">
                <div class="box-cover-right"
                     style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section4']->value['img'];?>
)"></div>
            </div>
            <!--LOCATION-->
            <!--FACILITIES-->
            <div class="group-right" data-name="facilities-home">
                <div class="box-cover-right"
                     style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section5']->value['img'];?>
)"></div>
            </div>
            <!--FACILITIES-->
            <!--APARTMENT-->
            <div class="group-right" data-name="apartemnt-home">
                <div class="box-cover-right"
                     style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['section6']->value['img'];?>
)"></div>
            </div>
            <!--APARTMENT-->
            <!--NEWS-->
            <div class="group-right" data-name="news-home">
                <div class="box-cover-right"></div>
            </div>
            <!--NEWS-->
            <!--CONTACT-->
            <div class="group-right" data-name="contact-home">
                <div class="box-cover-right"></div>
            </div>
            <!--CONTACT-->
        </div>
        <!--RIGHT CONTENT-->
    </div>
</section><?php }} ?>
