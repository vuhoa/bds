<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-09-30 09:40:49
         compiled from "application\views\templates\home\library.phtml" */ ?>
<?php /*%%SmartyHeaderCode:11617028805d916b31c14e92-38678652%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9b4f81a6bd8dbe9ee8a9048805c6db7cb8dd4ae0' => 
    array (
      0 => 'application\\views\\templates\\home\\library.phtml',
      1 => 1555660246,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11617028805d916b31c14e92-38678652',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'libraries' => 0,
    'item' => 0,
    'key' => 0,
    'libCategory' => 0,
    'lib' => 0,
    'videos' => 0,
    'vid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d916b31c57c46_82980208',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d916b31c57c46_82980208')) {function content_5d916b31c57c46_82980208($_smarty_tpl) {?><section class="container" id="library-page">
    <div class="title-page"><h1>Thư viện</h1></div>
    <div class="sub-nav">
        <ul>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['libraries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
            <li><a href="" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_title'];?>
"
                   data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_description'];?>
"
                   data-keyword="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_keywords'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></li>
            <?php } ?>

        </ul>
    </div>
    <!--SLIDE CONTENT-->
    <div class="slider-about">
        <div class="pagination"></div>
        <div class="slide-bg">
            <div class="item-wrapper">
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['libraries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                <?php if (($_smarty_tpl->tpl_vars['key']->value==0)) {?>
                <div class="box-library item-container" data-hash="<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
"
                     data-href="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/thu-vien/hinh-anh.html"
                     data-title="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_title'];?>
"
                     data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_description'];?>
"
                     data-keyword="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_keywords'];?>
">
                    <div class="bg-cover"
                         style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['item']->value['img'];?>
)"></div>
                    <h2><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</h2>
                    <div class="pic-center">
                        <?php  $_smarty_tpl->tpl_vars['lib'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lib']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['libCategory']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lib']->key => $_smarty_tpl->tpl_vars['lib']->value) {
$_smarty_tpl->tpl_vars['lib']->_loop = true;
?>
                        <div class="box-library-picture">
                            <div class="pic-library"><img
                                        src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['lib']->value['img'];?>
"
                                        alt="<?php echo $_smarty_tpl->tpl_vars['lib']->value['name'];?>
"></div>
                            <a class="view-album" href="javascript:void(0);"
                               data-href="<?php echo base_url('chi-tiet-thu-vien');?>
.html?num=<?php echo $_smarty_tpl->tpl_vars['lib']->value['id'];?>
"></a>
                            <div class="title-pic"><h3><?php echo $_smarty_tpl->tpl_vars['lib']->value['name'];?>
</h3></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php } else { ?>
                <div class="box-library item-container" data-hash="<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
"
                     data-href="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/thu-vien/video-2.html"
                     data-title="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_title'];?>
"
                     data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_description'];?>
"
                     data-keyword="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_keywords'];?>
">
                    <div class="bg-cover"
                         style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['item']->value['img'];?>
)"></div>
                    <h2><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</h2>
                    <div class="pic-center video-slide">
                        <?php  $_smarty_tpl->tpl_vars['vid'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['vid']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['videos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['vid']->key => $_smarty_tpl->tpl_vars['vid']->value) {
$_smarty_tpl->tpl_vars['vid']->_loop = true;
?>
                        <div class="box-library-video">
                            <div class="pic-library-video"><img
                                        src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['vid']->value['img'];?>
"
                                        alt="<?php echo $_smarty_tpl->tpl_vars['vid']->value['title'];?>
"><a
                                        class="player" href="javascript:void(0);"
                                        data-href="<?php echo base_url('chi-tiet-video');?>
.html?num=<?php echo $_smarty_tpl->tpl_vars['vid']->value['id'];?>
"></a></div>
                            <div class="title-video"><h3><?php echo $_smarty_tpl->tpl_vars['vid']->value['title'];?>
</h3></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php }?>
                <?php } ?>
            </div>
        </div>
    </div>
    <!--SLIDE CONTENT-->
</section><?php }} ?>
