<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-09-10 13:36:26
         compiled from "application\views\templates\layout.phtml" */ ?>
<?php /*%%SmartyHeaderCode:4126970805d77446a0d72e6-89842364%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af7122fe77edb92d58d9e036f0d7c9cf3e46b0b3' => 
    array (
      0 => 'application\\views\\templates\\layout.phtml',
      1 => 1567737980,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4126970805d77446a0d72e6-89842364',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'seo' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_image' => 0,
    'ext' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d77446a236be2_18390795',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d77446a236be2_18390795')) {function content_5d77446a236be2_18390795($_smarty_tpl) {?><!DOCTYPE HTML>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta http-equiv="cache-control" content="private">
    <meta http-equiv="Content-Language" content="vi">
    <meta name="google" content="notranslate">
    <meta name="language" content="vi_VN">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Florence Mỹ Đình - Nơi thịnh vượng an vui">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <title id="hdtitle"><?php echo $_smarty_tpl->tpl_vars['seo']->value['meta_title'];?>
</title>
    <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['seo']->value['meta_description'];?>
">
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['seo']->value['meta_keywords'];?>
">
    <!-- android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta http-equiv="cleartype" content="on">
    <!-- iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-title" content="<?php echo $_smarty_tpl->tpl_vars['seo']->value['meta_title'];?>
">
    <!-- Facebook -->
    <meta property="og:url" content="<?php echo current_url();?>
">
    <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
">
    <meta property="og:description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
">
    <meta property="og:image" content="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['meta_image']->value;?>
">
    <meta property="og:locale" content="vi_VN">
    <!-- Google Plus -->
    <meta itemprop="name" content="<?php echo $_smarty_tpl->tpl_vars['seo']->value['meta_title'];?>
">
    <meta itemprop="description" content="<?php echo $_smarty_tpl->tpl_vars['seo']->value['meta_title'];?>
">
    <meta itemprop="image" content="<?php echo base_url();?>
/images/<?php echo $_smarty_tpl->tpl_vars['ext']->value['logo']['img'];?>
">
    <link rel="icon" href="<?php echo base_url();?>
favicon.png">
    <?php echo link_tag('template/frontend/catalog/view/theme/default/css/style.css');?>


    <link rel="alternate" href="<?php echo base_url();?>
" hreflang="vi-vn">
    <link href="<?php echo base_url();?>
" rel="canonical">
    <?php echo link_tag('template/frontend/catalog/view/theme/default/css/validationEngine.jquery.css');?>


    <?php echo script_tag('template/frontend/catalog/view/theme/default/js/jquery.js');?>

    <!-- Google Analytics Code -->
    <!-- End Google Analytics Code -->
    <!-- Facebook Pixel Code -->
    <!-- End Facebook Pixel Code -->
</head>
<body>
<?php echo script_tag('template/frontend/catalog/view/theme/default/js/common.js');?>

<?php echo script_tag('template/frontend/catalog/view/theme/default/js/slide.js');?>

<?php echo script_tag('template/frontend/catalog/view/theme/default/js/scroll.js');?>

<?php echo script_tag('template/frontend/catalog/view/theme/default/js/load.js');?>

<?php echo script_tag('template/frontend/catalog/view/theme/default/js/validate.js');?>

<?php echo script_tag('template/frontend/catalog/view/theme/default/js/jquery.validationEngine.js');?>

<!--REGISTER-->
<!-- Global site tag (gtag.js) - Google Ads: 773397477 -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=UA-137045036-1"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-137045036-1');
<?php echo '</script'; ?>
>
<!-- Google Tag Manager -->
<?php echo '<script'; ?>
>
    (function (w, i, d, g, e, t, s) {
        w[d] = w[d] || [];
        t = i.createElement(g);
        t.async = 1;
        t.src = e;
        s = i.getElementsByTagName(g)[0];
        s.parentNode.insertBefore(t, s);
    })(window, document, '_gscq', 'script', '//widgets.getsitecontrol.com/131205/script.js');
<?php echo '</script'; ?>
>
<!--REGISTER-->
<!--LOAD-PAGE-->
<div class="all-pics"></div>
<div class="all-album"></div>
<div class="allvideo"></div>
<div class="overlay-dark"></div>
<!--LOAD-PAGE-->
<!--REGISTER-->
<div class="register-form">
    <a class="close" href="javascript:void(0);">close</a>
    <form method="POST" accept-charset="UTF-8" id="register" action="<?php echo base_url('lien-he');?>
.html">
        <div class="require-col">
            <h3>Nhận Thông tin Dự Án</h3>
            <div class="input-text">
                <input id="subscription_customer_attributes_full_name" type="text" placeholder="Họ tên Quý Khách"
                       class="quantumWizTextinputPaperinputInput exportInput" name="username" required />
            </div>
            <div class="input-text">
                <input id="subscription_customer_attributes_email" type="email" value="" placeholder="Email"
                       class="quantumWizTextinputPaperinputInput exportInput" name="email" required />
            </div>
            <div class="input-text">
                <input id="subscription_customer_attributes_phone" type="tel" value="" placeholder="Điện Thọai"
                       class="quantumWizTextinputPaperinputInput exportInput" name="phone" required />
            </div>
            <div class="input-but">
                <button class="but" id="btn-register-submit" type="submit">Đăng ký nhận thông tin</button>
            </div>
        </div>
    </form>
</div>
<!--REGISTER-->
<!--HEADER-->

    <?php echo $_smarty_tpl->getSubTemplate ("header.phtml", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['content']->value).".phtml", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate ("footer.phtml", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


</body>

</html>

<?php }} ?>
