<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-03 17:10:00
         compiled from "application\views\templates\home\about.phtml" */ ?>
<?php /*%%SmartyHeaderCode:8787261795d95c8f85f5d82-97039994%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ae6064fd51ce810a2355b48207cee7e5b0738e4' => 
    array (
      0 => 'application\\views\\templates\\home\\about.phtml',
      1 => 1555646878,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8787261795d95c8f85f5d82-97039994',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aboutCate' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d95c8f8671cd8_86388006',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d95c8f8671cd8_86388006')) {function content_5d95c8f8671cd8_86388006($_smarty_tpl) {?><section class="container" id="about-page">
    <div class="title-page"><h1>Giới thiệu</h1></div>
    <div class="box-nav">
        <ul>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aboutCate']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
            <li><a href="javascript:void(0);" data-dot="<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
"></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="box-slider">
        <!--LEFT CONTENT-->
        <div class="box-left">
            <!--SUB-MENU-->
            <div class="sub-nav">
                <ul>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aboutCate']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    <li><a href="javascript:void(0);" data-target="<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_title'];?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_description'];?>
" data-keyword="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_keywords'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></li>
                    <?php } ?>
                </ul>
            </div>
            <!--SUB-MENU-->
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aboutCate']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
            <div class="group-left" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
">
                <div class="box-cover"><img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
" alt="Giới thiệu dự án"></div>
                <div class="text-intro">
                    <h2><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</h2>
                    <div class="scrollA">
                        <div class="box-text">
                            <?php echo $_smarty_tpl->tpl_vars['item']->value['content'];?>

                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <!--LEFT CONTENT-->
        <!--RIGHT CONTENT-->
        <div class="box-right">
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aboutCate']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
            <div class="group-right" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
">
                <div class="box-cover-right" style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
)"></div>
            </div>
            <?php } ?>
        </div>
        <!--RIGHT CONTENT-->
    </div>
</section><?php }} ?>
