<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-03 17:10:14
         compiled from "application\views\templates\home\location.phtml" */ ?>
<?php /*%%SmartyHeaderCode:15878791475d95c906162fa1-03961232%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e6a2dcee415ed76742c408152678205d01da5a7' => 
    array (
      0 => 'application\\views\\templates\\home\\location.phtml',
      1 => 1555647736,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15878791475d95c906162fa1-03961232',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'location' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d95c9061da0b6_95786038',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d95c9061da0b6_95786038')) {function content_5d95c9061da0b6_95786038($_smarty_tpl) {?><section class="container" id="location-page">
    <div class="title-page"><h1>Vị trí</h1></div>
    <div class="box-slider">
        <!--LEFT CONTENT-->
        <div class="box-left">
            <!--LOCATION-->
            <div class="group-left" data-name="location">
                <div class="box-cover">
                    <div class="map-mobile">
                        <div class="compass"></div>
                        <img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['location']->value['image'];?>
" alt="VỊ TRÍ TRUNG TÂM">
                    </div>
                </div>
                <div class="text-intro">
                    <div class="box-text">
                        <?php echo $_smarty_tpl->tpl_vars['location']->value['content'];?>

                    </div>
                </div>
            </div>
            <!--LOCATION-->
        </div>
        <!--LEFT CONTENT-->
        <!--RIGHT CONTENT-->
        <div class="box-right">
            <!--LOCATION-->
            <div class="group-right" data-name="location">
                <div class="box-cover-right">
                    <div class="viewer">
                        <div class="compass"></div>
                        <div class="panzoom">
                            <div class="map-img">
                                <img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['location']->value['image'];?>
" alt="VỊ TRÍ TRUNG TÂM">
                            </div>
                        </div>
                        <div class="buttons">
                            <div class="mouse"></div>
                            <button class="pic-zoom-in">Zoom In</button>
                            <button class="pic-zoom-out">Zoom Out</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--LOCATION-->
        </div>
        <!--RIGHT CONTENT-->
        <div class="bot-circle">
            <div class="show-box" data-box="dot-01">
                <h3>Florence </h3>
                <img src="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/pictures/catalog/location/s1.jpeg" alt="Florence ">
            </div>
            <div class="show-box" data-box="dot-02">
                <h3>
                    <small>Bệnh Viện</small>
                    Pháp Việt
                </h3>
                <img src="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/pictures/catalog/location/s3.jpeg" alt="Pháp Việt">
            </div>
            <div class="show-box" data-box="dot-03">
                <h3>
                    <small>Trường ĐH</small>
                    RMIT
                </h3>
                <img src="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/pictures/catalog/location/s2.jpeg" alt="RMIT">
            </div>
            <div class="show-box" data-box="dot-04">
                <h3>Hồ Bán Nguyệt</h3>
                <img src="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/pictures/catalog/location/s6.jpeg" alt="Hồ Bán Nguyệt">
            </div>
            <div class="show-box" data-box="dot-05">
                <h3>
                    <small>TTTM</small>
                    Cresent Mall
                </h3>
                <img src="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/pictures/catalog/location/s5.jpeg" alt="Cresent Mall">
            </div>
            <div class="show-box" data-box="dot-06">
                <h3>
                    <small>TT Triển Lãm</small>
                    SECC
                </h3>
                <img src="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/pictures/catalog/location/s9.jpeg" alt="SECC">
            </div>
        </div>
    </div>
</section><?php }} ?>
