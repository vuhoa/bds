<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-03 17:10:10
         compiled from "application\views\templates\home\utility.phtml" */ ?>
<?php /*%%SmartyHeaderCode:17997229775d95c90205aeb0-68216207%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ab148d599221292f2ac01de668c0132d8d26890' => 
    array (
      0 => 'application\\views\\templates\\home\\utility.phtml',
      1 => 1555658291,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17997229775d95c90205aeb0-68216207',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'utilityContent' => 0,
    'utilities' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d95c9021266f8_12605897',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d95c9021266f8_12605897')) {function content_5d95c9021266f8_12605897($_smarty_tpl) {?><section class="container" id="library-page">
    <div class="title-page"><h1>Thư viện</h1></div>

    <!--SLIDE CONTENT-->
    <div class="slider-about">
        <div class="pagination"></div>
        <div class="slide-bg">
            <div class="item-wrapper">
                <div class="box-library item-container" data-hash="c-163"
                     data-href="<?php echo '<?php'; ?>
 echo $url; <?php echo '?>'; ?>
/thu-vien/hinh-anh.html"
                     data-title="<?php echo $_smarty_tpl->tpl_vars['utilityContent']->value['meta_title'];?>
"
                     data-description="<?php echo $_smarty_tpl->tpl_vars['utilityContent']->value['meta_description'];?>
"
                     data-keyword="<?php echo $_smarty_tpl->tpl_vars['utilityContent']->value['meta_keywords'];?>
">
                    <div class="bg-cover"
                         style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['utilityContent']->value['img'];?>
)"></div>
                    <h2><?php echo $_smarty_tpl->tpl_vars['utilityContent']->value['title'];?>
</h2>
                    <div class="pic-center">
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['utilities']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                        <div class="box-library-picture">
                            <div class="pic-library"><img
                                        src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['item']->value['img'];?>
"
                                        alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
"></div>
                            <a class="view-album" href="javascript:void(0);"
                               data-href="<?php echo base_url('chi-tiet-tien-ich');?>
.html?num=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"></a>
                            <div class="title-pic"><h3><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</h3></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--SLIDE CONTENT-->
</section><?php }} ?>
