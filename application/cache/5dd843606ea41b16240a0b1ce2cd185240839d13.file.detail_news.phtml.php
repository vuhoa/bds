<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-09-10 14:26:11
         compiled from "application\views\templates\home\detail_news.phtml" */ ?>
<?php /*%%SmartyHeaderCode:14997172355d77446a3482e6-34018741%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5dd843606ea41b16240a0b1ce2cd185240839d13' => 
    array (
      0 => 'application\\views\\templates\\home\\detail_news.phtml',
      1 => 1568100364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14997172355d77446a3482e6-34018741',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d77446a36f3e2_27419214',
  'variables' => 
  array (
    'newsContent' => 0,
    'news' => 0,
    'item' => 0,
    'newDetail' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d77446a36f3e2_27419214')) {function content_5d77446a36f3e2_27419214($_smarty_tpl) {?><div class="container" id="news-page">
    <div class="title-page"><h1>Tin tức</h1></div>
    <div class="box-library">
        <div class="bg-cover"
             style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['newsContent']->value['img'];?>
)"></div>
        <h2>Tin Dự án</h2>
        <div class="news-list">
            <div class="scrollB">
			
                   <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['news']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                        <div class="link-page">
							 <a href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['item']->value['alias'];?>
.html"
                               data-name="n-<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_title'];?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_keyword'];?>
" data-keyword="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_descriptions'];?>
">
								<div class="pic-thumb"><img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['item']->value['img'];?>
"
                                                        alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
">
								</div>
								<div class="title-h3-old"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</div>
							</a>
                        </div>
                  
                    <?php } ?>
				
            </div>
        </div>
        <!--NEWS LOAD-->
        <div class="colum-box-news">
            <a class="close-news" href="javascript:void(0);"></a>
            <div class="scrollC">
                <span class="click-hover"></span>
                <div class="news-content"></div>
            </div>
        </div>
        <!--NEWS LOAD-->
    </div>
</div>
<div class="newsid class-hidden"><?php echo $_smarty_tpl->tpl_vars['newDetail']->value['id'];?>
</div>
<?php }} ?>
