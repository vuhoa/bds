<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-03 17:09:54
         compiled from "application\views\templates\home\ground.phtml" */ ?>
<?php /*%%SmartyHeaderCode:6343744305d95c8f2c73ae5-02863533%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '983ea15196f2f362bf44b8e28f47ba0f9e0f0fa0' => 
    array (
      0 => 'application\\views\\templates\\home\\ground.phtml',
      1 => 1555653195,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6343744305d95c8f2c73ae5-02863533',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ground' => 0,
    'k' => 0,
    'item' => 0,
    'this' => 0,
    'groundDetail' => 0,
    'key' => 0,
    'itemD' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d95c8f376ba20_97428434',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d95c8f376ba20_97428434')) {function content_5d95c8f376ba20_97428434($_smarty_tpl) {?><section class="container" id="facilities-page">
    <div class="title-page"><h1>Mặt bằng dự án</h1></div>
    <div class="sub-nav">
        <ul>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ground']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <li><a href="" data-name="toa-r<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_title'];?>
"
                   data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_description'];?>
"
                   data-keyword="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_keywords'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></li>
            <?php } ?>
        </ul>
    </div>
    <!--SLIDE CONTENT-->
    <div class="slider-about">
        <div class="pagination"></div>
        <div class="slide-bg">
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ground']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <?php $_smarty_tpl->tpl_vars['groundDetail'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->b->__ground($_smarty_tpl->tpl_vars['item']->value['id']), null, 0);?>
            <div class="item-wrapper">
                <div class="facilities item-container" data-hash="toa-r<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
"
                     data-href=""
                     data-title="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_title'];?>
"
                     data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_description'];?>
"
                     data-keyword="<?php echo $_smarty_tpl->tpl_vars['item']->value['meta_keywords'];?>
">
                    <div class="facilities-map">
                        <h2><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</h2>
                        <div class="facilities-bg"
                             style="background-image:url(<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
)"></div>
                        <div class="compass"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</div>
                        <div class="all-dot">
                            <div class="all-dot-top">
                                <?php if (($_smarty_tpl->tpl_vars['k']->value===0)) {?>
                                <a class="dot-num dot-01" href="javascript:void(0);" data-name="01"
                                   data-box="01">01<span class="circle"></span></a>
                                <a class="dot-num dot-02" href="javascript:void(0);" data-name="02"
                                   data-box="02">02<span class="circle"></span></a>
                                <a class="dot-num dot-03 " href="javascript:void(0);" data-name="03"
                                   data-box="03">03</a>
                                <a class="dot-num dot-04" href="javascript:void(0);" data-name="04"
                                   data-box="04">05<span class="circle"></span></a>
                                <a class="dot-num dot-05" href="javascript:void(0);" data-name="05"
                                   data-box="05">06<span class="circle"></span></a>
                                <a class="dot-num dot-06 " href="javascript:void(0);" data-name="06"
                                   data-box="06">07</a>
                                <a class="dot-num dot-07 " href="javascript:void(0);" data-name="07"
                                   data-box="07">08</a>
                                <a class="dot-num dot-08" href="javascript:void(0);" data-name="08" data-box="08">09</a>
                                <a class="dot-num dot-09" href="javascript:void(0);" data-name="09" data-box="09">10</a>
                                <a class="dot-num dot-10" href="javascript:void(0);" data-name="10"
                                   data-box="10">11<span class="circle"></span></a>
                                <a class="dot-num dot-11" href="javascript:void(0);" data-name="11"
                                   data-box="11">12<span class="circle"></span></a>
                                <a class="dot-num dot-12" href="javascript:void(0);" data-name="12"
                                   data-box="12">15<span class="circle"></span></a>
                                <?php } else { ?>
                                <a class="dot-num dot-13 r2" href="javascript:void(0);" data-name="11"
                                   data-box="11">12<span class="circle"></span></a>
                                <a class="dot-num dot-14 r2" href="javascript:void(0);" data-name="12"
                                   data-box="12">15<span class="circle"></span></a>
                                <a class="dot-num dot-15 r2" href="javascript:void(0);" data-name="13"
                                   data-box="13">11<span class="circle"></span></a>
                                <a class="dot-num dot-16 r2" href="javascript:void(0);" data-name="14"
                                   data-box="14">10<span class="circle"></span></a>
                                <a class="dot-num dot-17 r2" href="javascript:void(0);" data-name="15"
                                   data-box="15">01<span class="circle"></span></a>
                                <a class="dot-num dot-18 r2" href="javascript:void(0);" data-name="16"
                                   data-box="16">02<span class="circle"></span></a>
                                <a class="dot-num dot-19 r2" href="javascript:void(0);" data-name="17"
                                   data-box="17">03<span class="circle"></span></a>
                                <a class="dot-num dot-20 r2" href="javascript:void(0);" data-name="18"
                                   data-box="18">05<span class="circle"></span></a>
                                <a class="dot-num dot-21 r2" href="javascript:void(0);" data-name="19"
                                   data-box="19">06<span class="circle"></span></a>
                                <a class="dot-num dot-22 r2" href="javascript:void(0);" data-name="20"
                                   data-box="20">07<span class="circle"></span></a>
                                <a class="dot-num dot-23 r2" href="javascript:void(0);" data-name="21"
                                   data-box="21">08<span class="circle"></span></a>
                                <a class="dot-num dot-24 r2" href="javascript:void(0);" data-name="22"
                                   data-box="22">09<span class="circle"></span></a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <!--SHOW BOX-->
                    <div class="info-facilities">
                        <?php  $_smarty_tpl->tpl_vars['itemD'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemD']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['groundDetail']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemD']->key => $_smarty_tpl->tpl_vars['itemD']->value) {
$_smarty_tpl->tpl_vars['itemD']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['itemD']->key;
?>
                        <?php if (($_smarty_tpl->tpl_vars['k']->value==0)) {?>
                            <?php if (($_smarty_tpl->tpl_vars['key']->value<9)) {?>
                                <div class="show-box-pic" data-pic="0<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
">
                            <?php } else { ?>
                                 <div class="show-box-pic" data-pic="<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
">
                            <?php }?>
                        <?php } else { ?>
                             <div class="show-box-pic" data-pic="<?php echo $_smarty_tpl->tpl_vars['key']->value+15;?>
">
                        <?php }?>

                            <img src="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['itemD']->value['img'];?>
"
                                 data-srcl="<?php echo base_url();?>
images/<?php echo $_smarty_tpl->tpl_vars['itemD']->value['img'];?>
"
                                 alt="<?php echo $_smarty_tpl->tpl_vars['itemD']->value['title'];?>
">
                            <div class="number">R1-<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
</div>
                            <div class="faci-text">
                                <h3><?php echo $_smarty_tpl->tpl_vars['itemD']->value['name'];?>
</h3>
                                <?php echo $_smarty_tpl->tpl_vars['itemD']->value['content'];?>

                            </div>
                        </div>
                        <?php } ?>

                    </div>
                    <!--SHOW BOX-->
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <!--SLIDE CONTENT-->
</section><?php }} ?>
