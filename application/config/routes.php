<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$route['default_controller'] = "front/home";
$route['trang-chu'] = "front/home";
//$route['404_override'] = 'error';
$route['admin'] = "admin/admin/index";
$route['admin/login'] = "admin/admin/login";
$route['admin/logout'] = "admin/admin/logout";
$route['admin/order'] = "admin/admin/order";
$route['admin/comment'] = "admin/admin/comment";
/*
 * Custom route
 */
$route['404_override'] = 'error';
require_once( BASEPATH . 'database/DB' . EXT );
$db = & DB();
/*
 * Custom Menu/Route
 */


$db->select('alias');
$db->where('status', 1);
//$db->where('parent_id !=', 0);
$sql = $db->get('post');
foreach ($sql->result_array() as $row) {
    $route[$row['alias']] = 'front/home/detail_news/' . $row['alias'];
}
//

$route['gioi-thieu'] = 'front/home/about';
$route['vi-tri-du-an'] = 'front/home/location';
$route['tien-ich'] = 'front/home/utility';
$route['mat-bang-can-ho'] = 'front/home/ground';
$route['tin-tuc-su-kien'] = 'front/home/news';
$route['tien-do-du-an'] = 'front/home/tiendo';
$route['thu-vien-tai-lieu'] = 'front/home/library';
$route['lien-he'] = 'front/home/contact';
//
$route['chi-tiet-tai-lieu'] = 'front/home/docs';
$route['chi-tiet-can-ho'] = 'front/home/apartment';
$route['chi-tiet-tien-ich'] = 'front/home/sample';
$route['chi-tiet-thu-vien'] = 'front/home/lib';
$route['chi-tiet-video'] = 'front/home/video';
$route['chi-tiet-tin'] = 'front/home/detail_news';

//$route['lay-bo-so'] = 'front/home/getBoSo';
//$route['xac-nhan/(:num)/(:num)/(:num)'] = 'front/home/xacnhan/$1/$1/$1';
//$route['dieu-hoa'] = 'front/product/index';
//$route['dieu-hoa/(:num)'] = 'front/product/index/$1';
//$route['video-tap-doan'] = 'front/home/video';
//$route['hinh-anh-tap-doan'] = 'front/home/image';
//$route['giai-tri'] = 'front/home/entertainment';
//$route['ban-tin-noi-bo'] = 'front/home/news';
//$route['cap-nhat-ke-hoach'] = 'front/home/capnhatkehoach';
//$route['cap-nhat-ban-tin'] = 'front/home/capnhatbanttin';

