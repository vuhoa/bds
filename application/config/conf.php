<?php

/**
 * Description of conf
 * @author trungthuc
 * @date Jul 25, 2015
 */
##########################################
$config['pathImg'] = 'images';
$config['pathThumbs'] = '_thumbs';
$config['pathFile'] = 'files'; 
$config['wCrop'] = 200;
$config['hCrop'] = 200;
##########################################
$config['title'] = '';
$config['descriptions'] = '';
$config['keywords'] = '';
$config['email'] = '';
$config['hotline'] = '';

