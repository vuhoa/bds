<?php

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 28, 2015
 */
class groups extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public $group = 'role';

    function __totalGroups() {
        return $this->db->count_all_results($this->group);
    }

    function __getGroups($limit = null, $offset = 0) {
        $this->db->select('*');
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get($this->group);
        return $query->result_array();
        $query->free_result();
    }

    function __saveGroups() {
        //Check object
        $data = array(
            'name' => $this->input->get_post('name'),
            'status' => 1
        );
        $this->db->set('create_by', $this->session->userdata['user_id']);
        $this->db->set('create_time', 'NOW()', FALSE);
        $this->db->insert($this->group, $data);
        return $this->db->insert_id();
    }

    function __detailGroups($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->group);
        foreach ($query->result_array() as $result)
            return $result;
        $query->free_result();
    }

    function __updateGroups($id) {
        $data = array(
            'name' => $this->input->get_post('name'),
            'level' => $this->input->get_post('level'),
        );
        $this->db->where('id', $id);
        $this->db->update($this->group, $data);
    }

    function __deleteGroups($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->group);
    }

}
