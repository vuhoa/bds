<?php

/**
 * Description of b
 * @author trungthuc
 * @date Jul 10, 2016
 */
class b extends CI_Model {
    
    public $cfg;

    function __construct() {
        parent::__construct();
        $this->load->config('conf');
        $this->cfg = (Object) $this->config->config;
    }
    
    function __menuTops(){
        $this->db->select('*');
        $this->db->where('position', 1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('menu');
        return $sql->result_array();
        $sql->free_result();
    }

    function __menu($pid){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('parent_id', $pid);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('menu_cate');
        return $sql->result_array();
        $sql->free_result();
    }

    function __ground($cate){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('cate', $cate);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('ground');
        return $sql->result_array();
        $sql->free_result();
    }

    function __libCategory(){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('library');
        return $sql->result_array();
        $sql->free_result();
    }

    function __menu_home(){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('parent_id !=', 0);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('menu_cate');
        return $sql->result_array();
        $sql->free_result();
    }

    function __menuHome(){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('parent_id', 0);
        $this->db->where('menu', 0);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('menu_cate');
        return $sql->row_array();
        $sql->free_result();
    }

    function __menuContent($slug){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('slug',$slug);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('menu_cate');
        return $sql->row_array();
        $sql->free_result();
    }

    function _getHomeDocs($cate){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('home',1);
        $this->db->where('cate',$cate);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('library');
        return $sql->row_array();
        $sql->free_result();
    }

    function _getHomeVideo(){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('home',1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('video');
        return $sql->row_array();
        $sql->free_result();
    }

    function __menuContentID($id){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->where('id',$id);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('menu_cate');
        return $sql->row_array();
        $sql->free_result();
    }

    function __getKeHoach(){
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'DESC');
        $sql = $this->db->get('kehoach');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getLastORrderKH(){
        $this->db->select('ord');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'DESC');
        $sql = $this->db->get('kehoach');
        return $sql->row_array();
        $sql->free_result();
    }

    function __getBanTin(){
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'DESC');
        $sql = $this->db->get('news');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getLastORrderNews(){
        $this->db->select('ord');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'DESC');
        $sql = $this->db->get('news');
        return $sql->row_array();
        $sql->free_result();
    }

    function __getImages(){
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'DESC');
        $sql = $this->db->get('images');
        return $sql->row_array();
        $sql->free_result();
    }

    function __getSpecialImages($date){
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('date', $date);
        $this->db->order_by('ord', 'DESC');
        $sql = $this->db->get('special_img');
        return $sql->result_array();
        $sql->free_result();
    }

    function __categoryContent($slug){
        $this->db->select('*');
        $this->db->where('slug',$slug);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('category');
        return $sql->row_array();
        $sql->free_result();
    }

    function __categoryNewsContent($slug){
        $this->db->select('*');
        $this->db->where('slug',$slug);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('cate_news');
        return $sql->row_array();
        $sql->free_result();
    }

    function __slider($right){
        $this->db->select(array('id','name','link','img'));
        $this->db->where('right', $right);
        $this->db->where('footer', 0);
        $this->db->where('banner', 0);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('slide');
        return $sql->result_array();
        $sql->free_result();
    }

    function __sliderFooter(){
        $this->db->select(array('id','name','link','img'));
        $this->db->where('footer', 1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('slide');
        return $sql->row_array();
        $sql->free_result();
    }

    function __sliderLogo(){
        $this->db->select(array('id','name','link','img'));
        $this->db->where('logo', 1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('slide');
        return $sql->row_array();
        $sql->free_result();
    }

    function __sliderBanner(){
        $this->db->select(array('id','name','link','img'));
        $this->db->where('banner', 1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('slide');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getSEO()
    {
        $this->db->select('*');
        $sql = $this->db->get('seo');
        return $sql->row_array();
        $sql->free_result();
    }

    function __getSocial(){
        $this->db->select('*');
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('social');
        return $sql->result_array();
        $sql->free_result();
    }

    function __partners(){
        $this->db->select(array('id','name','link','img'));
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('partner');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getReason(){
        $this->db->select(array('id','name','more','img'));
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('reason');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getAbout(){
        $this->db->select(array('id','name','more','img'));
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('about');
        return $sql->result_array();
        $sql->free_result();
    }

    function __video(){
        $this->db->select('*');
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('video');
        return $sql->result_array();
        $sql->free_result();
    }

    function __support(){
        $this->db->select(array('id','name','phone','email'));
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('online_support');
        return $sql->result_array();
        $sql->free_result();
    }

    function __category(){
        $this->db->select('*');
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('category');
        return $sql->result_array();
        $sql->free_result();
    }

    function __solution($pid){
        $this->db->select('id,img,name');
        $this->db->where('parent_id',$pid);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('solution');
        return $sql->result_array();
        $sql->free_result();
    }

    function __services(){
        $this->db->select('id,img,name,title,more');
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('service');
        return $sql->result_array();
        $sql->free_result();
    }

    function __condition(){
        $this->db->select('id,name');
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('condition');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getLinks($id){
        $this->db->select('id,name,title,img,more,link');
        $this->db->where('id',$id);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('links');
        return $sql->row_array();
        $sql->free_result();
    }

    function getAccount($phone){
        $this->db->select('phone,total');
        $this->db->where('phone',$phone);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('links');
        return $sql->row_array();
        $sql->free_result();
    }

    function __links(){
        $this->db->select('id,name,link,slug');
        $this->db->where('footer',1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('links');
        return $sql->result_array();
        $sql->free_result();
    }

    function __subjects(){
        $this->db->select('id,name');
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('subject');
        return $sql->result_array();
        $sql->free_result();
    }

    function __categoryNews(){
        $this->db->select('*');
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('cate_news');
        return $sql->result_array();
        $sql->free_result();
    }

    function __typeProducts($type){
        $this->db->select('*');
        $this->db->where('type',$type);
        $this->db->where('home',1);
        $this->db->order_by('ord', 'ASC');
        $this->db->limit(8);
        $sql = $this->db->get('product');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getProduct($limit = null, $start = 0) {
        $this->db->select(array('id','img','name','slug','new_price','old_price'));
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'ASC');
        if ($limit)
            $this->db->limit($limit, $start);
        $sql = $this->db->get('product');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getProductSearch($limit = null, $start = 0, $search) {
        $this->db->select(array('id','img','name','slug','new_price','old_price'));
        $this->db->where('status', 1);
        $this->db->like('name', $search,'both');
        $this->db->order_by('ord', 'ASC');
        if ($limit)
            $this->db->limit($limit, $start);
        $sql = $this->db->get('product');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getNews() {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('post');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getUtility() {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('utility');
        return $sql->result_array();
        $sql->free_result();
    }

    function _getUtilityDetail($id) {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('id', $id);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('utility');
        return $sql->row_array();
        $sql->free_result();
    }

    function _getVideoDetail($id) {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('id', $id);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('video');
        return $sql->row_array();
        $sql->free_result();
    }

    function _getLibraryDetail($id) {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('id', $id);
        $this->db->order_by('ord', 'ASC');
        $sql = $this->db->get('library');
        return $sql->row_array();
        $sql->free_result();
    }

    function __getProductByCate($limit = null, $start = 0,$cate) {
        $this->db->select(array('id','img','name','slug','new_price','old_price'));
        $this->db->where('status', 1);
        $this->db->where_in('cate', $cate);
        $this->db->order_by('ord', 'ASC');
        if ($limit)
            $this->db->limit($limit, $start);
        $sql = $this->db->get('product');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getNewsByCate($limit = null, $start = 0,$cate) {
        $this->db->select(array('id','img','name','alias','create_time','more', 'content'));
        $this->db->where('status', 1);
        $this->db->where_in('cate', $cate);
        $this->db->order_by('ord', 'ASC');
        if ($limit)
            $this->db->limit($limit, $start);
        $sql = $this->db->get('post');
        return $sql->result_array();
        $sql->free_result();
    }

    function __save_contact($data,$by)
    {
        $this->db->set('create_time', Date('Y-m-d H:i:s'));
        $this->db->set('create_by', $by);
        $this->db->set('status', 0);
        $this->db->insert('lienhe', $data);
        return $this->db->insert_id();
    }
    function __save_account($data,$by)
    {
//        print_r($this->db->insert_id());
//        die();
        $this->db->set('create_time', Date('Y-m-d H:i:s'));
        $this->db->set('create_by', $by);
        $this->db->set('status', 0);
        $this->db->insert('links', $data);
        return $this->db->insert_id();
    }


    function __menu_top() {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('parent_id', 0);
        $this->db->order_by('ord', 'ASC');
        $query = $this->db->get('category');
        $html = '';
        foreach ($query->result_array() as $row) {
            $html .= '<li>';
            $html .= anchor($row['slug'], $row['name']);
            if ($row['parent_id'] == 0) {
                $this->db->select('*');
                $this->db->where('status', 1);
                $this->db->where('parent_id', $row['id']);
                $this->db->order_by('ord', 'ASC');
                $query = $this->db->get('category');
                $rows = $query->result_array();
                if (count($rows)) {
                    $html .= '<ul class="submenu">';
                    foreach ($rows as $dt) {
                        $html .= '<li>';
                        $html .= anchor($dt['slug'], $dt['name']);
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                }
            }
            $html .= '</li>';
        }
        $query->free_result();
        return $html;
    }

    function __menu_top_nobile() {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('parent_id', 0);
        $this->db->order_by('ord', 'ASC');
        $query = $this->db->get('category');
        $html = '';
        foreach ($query->result_array() as $row) {
            $html .= '<li class="menu__item">';
            $html .= anchor($row['slug'], $row['name']);
            if ($row['parent_id'] == 0) {
                $this->db->select('*');
                $this->db->where('status', 1);
                $this->db->where('parent_id', $row['id']);
                $this->db->order_by('ord', 'ASC');
                $query = $this->db->get('category');
                $rows = $query->result_array();
                if (count($rows)) {
                    $html .= '<ul class="menu-sub">';
                    foreach ($rows as $dt) {
                        $html .= '<li class="menu-sub__item">';
                        $html .= anchor($dt['slug'], $dt['name']);
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                }
            }
            $html .= '</li>';
        }
        $query->free_result();
        return $html;
    }

    function __get_extension() {
        $this->db->select('*');
        $query = $this->db->get('tb_extension');
        $data = array();
        foreach ($query->result_array() as $row) {
            $data[$row['name']] = array(
                'link' => $row['name'],
                'img' => $row['img'],
                'content' => $row['content'],
                'link_left' => $row['link_left'],
                'img_left' => $row['img_left'],
                'link_right' => $row['link_right'],
                'img_right' => $row['img_right']
            );
        }
        return $data;
        $query->free_result();
    }

    function __detailProduct($alias) {
        $this->db->select('*');
        $this->db->where('slug', $alias);
        $sql = $this->db->get('product');
        return $sql->row_array();
        $sql->free_result();
    }

    function __detailNews($alias) {
        $this->db->select('*');
        $this->db->where('alias', $alias);
        $sql = $this->db->get('post');
        return $sql->row_array();
        $sql->free_result();
    }

    function __relatedProduct($id,$cate) {
        $this->db->select('*');
        $this->db->where('id !=', $id);
        $this->db->where('cate', $cate);
        $sql = $this->db->get('product');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getHotNews() {
        $this->db->select('*');
        $this->db->where('hotnews', 1);
        $this->db->limit(8);
        $sql = $this->db->get('post');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getCustomer($phone) {
        $this->db->select('*');
        $this->db->where('phone', $phone);
        $sql = $this->db->get('lienhe');
        return $sql->result_array();
        $sql->free_result();
    }

    function __getCustomerTotal($phone) {
        $this->db->select_sum('price');
        $this->db->where('status',1);
        $this->db->where('phone',$phone);
        $sql = $this->db->get('lienhe');
        return $sql->row_array();
        $sql->free_result();
    }

    function __detail_category_id($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $sql = $this->db->get('category');
        return $sql->row_array();
        $sql->free_result();
    }

    function __detail_category_slug($alias) {
        $this->db->select('*');
        $this->db->where('alias', $alias);
        $sql = $this->db->get('category');
        return $sql->row_array();
        $sql->free_result();
    }

    function __arrayCategory($id) {
        $this->db->select('id');
        $this->db->where('parent_id', $id);
        $sql = $this->db->get('category');
        $ids = array($id);
        foreach ($sql->result_array() as $row) {
            $ids[] = $row['id'];
        }
        return $ids;
        $sql->free_result();
    }

    function _newsHome($cate = 0, $limit = null) {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('cate', $cate);
        $this->db->order_by('ord', 'ASC');
        if ($limit)
            $this->db->limit($limit);
        $query = $this->db->get('post');
        return $query->result_array();
        $query->free_result();
    }
    function _support($limit = null) {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'ASC');
        if ($limit)
            $this->db->limit($limit);
        $query = $this->db->get('tuvan');
        return $query->result_array();
        $query->free_result();
    }

}
