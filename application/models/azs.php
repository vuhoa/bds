<?php

/**
 * Description of azs
 * @author trungthuc
 * @date Mar 23, 2015
 */
class azs extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function __support() {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->order_by('ord');
		$sql = $this->db->get('hotro');
		return $sql->result_array();
		$sql->free_result();
	}

	function __slide() {
		$this->db->select('*');
		$this->db->where('status', 1);
		$sql = $this->db->get('slide');
		return $sql->result_array();
		$sql->free_result();
	}

	function __detail_category($alias) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->where('alias', $alias);
		$sql = $this->db->get('category');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __partner() {
		$this->db->select('*');
		$this->db->where('status', 1);
		$sql = $this->db->get('partner');
		return $sql->result_array();
		$sql->free_result();
	}

	function __products($limit = null) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		if ($limit)
			$this->db->limit($limit);
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('product');
		return $sql->result_array();
		$sql->free_result();
	}

	function __detail_product($alias) {
		$this->db->select('*');
		$this->db->where('product.alias', $alias);
		$sql = $this->db->get('product');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __detail_post($alias) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->select('category.banner as cate_banner');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.category');
		$this->db->where('post.alias', $alias);
		$sql = $this->db->get();
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __product_same_cate($id, $limit = null) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where_not_in('id', $id);
		if ($limit)
			$this->db->limit($limit);
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('product');
		return $sql->result_array();
		$sql->free_result();
	}

	function __detail_cate_new($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('category');
		return $sql->row_array();
		$sql->free_result();
	}

	function __detail_cate_new_id($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$sql = $this->db->get('category');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __news_by_cate($cate, $limit = null, $start = null) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.category');
		$this->db->where('post.lang', $this->session->userdata('language'));
		$this->db->where('post.category', $cate);
		$this->db->where('post.status', 1);
		//$this->db->order_by('post.ord');
		$this->db->order_by('post.id', 'DESC');
		if ($limit)
			$this->db->limit($limit, $start);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __total_news_by_cate($cate) {
		$this->db->where('status', 1);
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('category', $cate);
		$this->db->from('post');
		return $this->db->count_all_results();
	}

	function __news_same_cate($cate, $id, $limit = null) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.category');
		$this->db->where('post.category', $cate);
		$this->db->where('post.status', 1);
		$this->db->where_not_in('post.id', $id);
		$this->db->order_by('post.ord');
		$this->db->order_by('post.id', 'DESC');
		$this->db->limit($limit);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __getAllcate($id) {
		$this->db->select('id');
		$this->db->where('parent_id', $id);
		$query = $this->db->get('category_product');
		$return = array($id);
		foreach ($query->result_array() as $row) {
			$return[] = $row['id'];
		}
		return $return;
	}

	function __find($key, $limit = null) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.category');
		$this->db->like('post.name', $key);
		$this->db->where('post.lang', $this->session->userdata('language'));
		$this->db->where('post.status', 1);
		$this->db->order_by('post.id', 'DESC');
		if ($limit)
			$this->db->limit($limit);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __detail_post_id($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$sql = $this->db->get('post');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function menu_top($top = 0, $check = null) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('top', $top);
		if ($check)
			$this->db->where('parent_id', 0);
		$sql = $this->db->get('category');
		return $sql->result_array();
		$sql->free_result();
	}

	function get_files_product($id, $limit = null) {
		$this->db->select('file.*');
		$this->db->from('file');
		$this->db->join('file_mapper', 'file_mapper.file_id = file.id', 'left');
		$this->db->where('file_mapper.status', 1);
		$this->db->where('file_mapper.record_id', $id);
		$this->db->where('file_mapper.object_id', 2);
		if ($limit)
			$this->db->limit($limit);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __agency() {
		$this->db->select('*');
		$this->db->where('status', 1);
		$sql = $this->db->get('agency');
		return $sql->result_array();
		$sql->free_result();
	}

	function menu_parent($id) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('parent_id', $id);
		$this->db->order_by('ord', 'DESC');
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('category');
		return $sql->result_array();
		$sql->free_result();
	}

	function __library($type = null) {
		$this->db->select('*');
		$this->db->where('status', 1);
		if ($type) {
			$this->db->where('video', 1);
		} else {
			$this->db->where('video', 0);
		}

		$sql = $this->db->get('library');
		return $sql->result_array();
		$sql->free_result();
	}

	function __checkParent($id) {
		$this->db->select('id');
		$this->db->where('parent_id', $id);
		$this->db->limit(1);
		$sql = $this->db->get('category');
		foreach ($sql->result_array() as $row)
			return $row['id'];
		$sql->free_result();
	}

	function __cosmetic() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('cosmetic');
		return $sql->result_array();
		$sql->free_result();
	}

	function __cosmetic_detail($alias) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('alias', $alias);
		$sql = $this->db->get('cosmetic');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __jscosmetic_first() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$sql = $this->db->get('cosmetic');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __detail_jscosmetic($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('cosmetic');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __total_news($cateId) {
		$this->db->where('status', 1);
		$this->db->where('category', $cateId);
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->from('news');
		return $this->db->count_all_results();
	}

	function __news($cateId, $limit = null, $start = null) {
		$this->db->select('*');
		$this->db->where('category', $cateId);
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'DESC');
		if ($limit)
			$this->db->limit($limit, $start);
		$sql = $this->db->get('news');
		return $sql->result_array();
		$sql->free_result();
	}

	function __detail_new($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('news');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __news_sames($id, $limit = null) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->where_not_in('id', $id);
		$this->db->order_by('ord');
		$this->db->order_by('id', 'DESC');
		$this->db->limit($limit);
		$sql = $this->db->get('news');
		return $sql->result_array();
		$sql->free_result();
	}

	function __category_first($category_id) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('parent_id', $category_id);
		$this->db->order_by('ord', 'DESC');
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$sql = $this->db->get('category');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __agencies() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('agencys');
		return $sql->result_array();
		$sql->free_result();
	}

	function __infocontact() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$sql = $this->db->get('infocontact');
		return $sql->result_array();
		$sql->free_result();
	}

	function __main_contact() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('main', 1);
		$this->db->limit(1);
		$sql = $this->db->get('infocontact');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __sub_contact() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('main', 0);
		$sql = $this->db->get('infocontact');
		return $sql->result_array();
		$sql->free_result();
	}

	function __footer($key = null) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		if ($key) {
			$this->db->where($key, 1);
		}
		$this->db->limit(1);
		$sql = $this->db->get('footer');
		foreach ($sql->result_array() as $row) {
			if ($row['text'])
				return $row['text'];
			else
				return '';
		}
		$sql->free_result();
	}

	function __hotline() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$sql = $this->db->get('hotline');
		return $sql->result_array();
		$sql->free_result();
	}

	function __about_us() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('about');
		return $sql->result_array();
		$sql->free_result();
	}

	function __cateNews() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('category_news');
		return $sql->result_array();
		$sql->free_result();
	}

	function __cateProducts() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('category_product');
		return $sql->result_array();
		$sql->free_result();
	}

	function __services() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('service');
		return $sql->result_array();
		$sql->free_result();
	}

	function __cateSupports() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('category_support');
		return $sql->result_array();
		$sql->free_result();
	}

	function __allProduct($limit = 15) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		if ($limit)
			$this->db->limit($limit);
		$sql = $this->db->get('product');
		return $sql->result_array();
		$sql->free_result();
	}

	function __cateByProduct($category, $limit = 15) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('category', $category);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		if ($limit)
			$this->db->limit($limit);
		$sql = $this->db->get('product');
		return $sql->result_array();
		$sql->free_result();
	}

	function __cateByNews($limit = 4) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('home', 1);
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		if ($limit)
			$this->db->limit($limit);
		$sql = $this->db->get('news');
		return $sql->result_array();
		$sql->free_result();
	}

	function __aboutDetail($alias) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('alias', $alias);
		$sql = $this->db->get('about');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __aboutDetailNew() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$sql = $this->db->get('about');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __HotNews($limit = 5) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		if ($limit)
			$this->db->limit($limit);
		$sql = $this->db->get('news');
		return $sql->result_array();
		$sql->free_result();
	}

	function __newHot() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$sql = $this->db->get('news');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __newHots($limit = 4, $offset = 1) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		if ($limit)
			$this->db->limit($limit, $offset);
		$sql = $this->db->get('news');
		return $sql->result_array();
		$sql->free_result();
	}

	function __detailNewCate($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('category_news');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __detailProductCate($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('category_product');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __productLimit($cateId, $limit = null, $start = null) {
		$this->db->select('*');
		$this->db->where('category', $cateId);
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'DESC');
		if ($limit)
			$this->db->limit($limit, $start);
		$sql = $this->db->get('product');
		return $sql->result_array();
		$sql->free_result();
	}

	function __getProductImg($imgId) {
		$this->db->select('f.file_path, fm.file_description');
		$this->db->from('file_mapper as fm');
		$this->db->join('file as f', 'f.id = fm.file_id');
		$this->db->where('fm.status', 1);
		$this->db->where('fm.record_id', $imgId);
		$this->db->where('fm.field_id', 304);
		$this->db->where('fm.object_id', 2);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __servicies() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$sql = $this->db->get('service');
		return $sql->result_array();
		$sql->free_result();
	}

	function __serviceDetail($alias) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('alias', $alias);
		$sql = $this->db->get('service');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __careers() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$sql = $this->db->get('career');
		return $sql->result_array();
		$sql->free_result();
	}

	function __careerDetail($alias) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('alias', $alias);
		$sql = $this->db->get('career');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __supports() {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$sql = $this->db->get('category_support');
		return $sql->result_array();
		$sql->free_result();
	}

	function __supportDetail($alias) {
		$this->db->select('*');
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$this->db->where('alias', $alias);
		$sql = $this->db->get('category_support');
		foreach ($sql->result_array() as $row)
			return $row;
		$sql->free_result();
	}

	function __questionQA($cateId) {
		$this->db->select('*');
		$this->db->where('category', $cateId);
		$this->db->where('lang', $this->session->userdata('language'));
		$this->db->where('status', 1);
		$sql = $this->db->get('supports');
		return $sql->result_array();
		$sql->free_result();
	}

	function __Province() {
		$this->db->select('*');
		$this->db->where('parent_id', 0);
		$this->db->where('status', 1);
		$this->db->order_by('name', 'ASC');
		$sql = $this->db->get('country');
		return $sql->result_array();
		$sql->free_result();
	}

	function __DistrictByPro($parentId) {
		$this->db->select('*');
		$this->db->where('parent_id', $parentId);
		$this->db->where('status', 1);
		$this->db->order_by('name', 'ASC');
		$sql = $this->db->get('country');
		return $sql->result_array();
		$sql->free_result();
	}

	function __agencieByDistrict($province = 0, $type = 0) {
		$this->db->select('*');
		if ($province)
			$this->db->where('province', $province);
		if ($type)
			$this->db->where('category', $type);
		$this->db->where('status', 1);

		$sql = $this->db->get('agencys');
		return $sql->result_array();
		$sql->free_result();
	}
	
	function __get_extension() {
		$this->db->select('*');
		$query = $this->db->get('tb_extension');
		$data = array();
		foreach ($query->result_array() as $row) {
			$data[$row['name']] = array(
				'link' => $row['name'],
				'img' => $row['img'],
				'content' => $row['content'],
				'link_left' => $row['link_left'],
				'img_left' => $row['img_left'],
				'link_right' => $row['link_right'],
				'img_right' => $row['img_right']
			);
		}
		return $data;
		$query->free_result();
	}

}
