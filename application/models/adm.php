<?php

/**
 * Description of adm
 * @author trungthuc
 * @date Jan 26, 2015
 */
class adm extends CI_Model {

    public $role = 'role';
    public $user = 'user';

    function __construct() {

        parent::__construct();
    }

    function getUser($user, $pass) {
        $this->db->select('*');
        $this->db->where('username', $user);
        $this->db->where('password', md5($pass));
        $this->db->limit(1);
        $query = $this->db->get('user');
        if ($query) {
            return $query->row_array();
        } else {
            return false;
        }
        $query->free_result();
    }

    function __getRole() {
        $data = array();
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('name');
        $query = $this->db->get($this->role);
        foreach ($query->result_array() as $row) {
            $data[$row['id']] = $row['name'];
        }
        return $data;
        $query->free_result();
    }

    function __getRoles() {
        $data = array();
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('name');
        $query = $this->db->get($this->role);
        return $query->result_array();
        $query->free_result();
    }

    function __getObjects() {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('object');
        return $query->result_array();
        $query->free_result();
    }

    function __checkAlias($object, $field, $value) {
        $this->db->select($field['name']);
        $this->db->where($field['name'], $value);
        $result = $this->db->get($object['name']);
        if ($result->result_array())
            return true;
        else
            return false;
    }

    function __totalTranslates() {
        return $this->db->count_all_results('lang');
    }

    function __getTranslates($limit = null, $offset = 0) {
        $this->db->select('*');
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('lang');
        return $query->result_array();
        $query->free_result();
    }

    function __deleteTranslates($id) {
        $this->db->where('id', $id);
        $this->db->delete('lang');
    }

    function __detailTranslates($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('lang');
        foreach ($query->result_array() as $result)
            return $result;
        $query->free_result();
    }

    function __editTranslates($id) {
        $data = array(
            'name_vi' => $this->input->get_post('name_vi'),
            'name_en' => $this->input->get_post('name_en'),
            'name_fr' => $this->input->get_post('name_fr')
        );
        $this->db->where('id', $id);
        $this->db->update('lang', $data);
    }

    function __saveTranslates() {
        //Check object
        $data = array(
            'name_vi' => $this->input->get_post('name_vi'),
            'name_en' => $this->input->get_post('name_en'),
            'name_fr' => $this->input->get_post('name_fr')
        );
        $this->db->set('create_by', $this->session->userdata['user_id']);
        $this->db->set('create_time', 'NOW()', FALSE);
        $this->db->insert('lang', $data);
        return $this->db->insert_id();
    }

    function __order($limit = null) {
        $this->db->select('*');
        if ($limit)
            $this->db->limit($limit);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('order');
        return $query->result_array();
        $query->free_result();
    }

    function __order_product($order_id) {
        $this->db->select('*');
        $this->db->where('order_id', $order_id);
        $query = $this->db->get('order_product');
        return $query->result_array();
        $query->free_result();
    }

    function __comment($limit = null) {
        $this->db->select('*');
        if ($limit)
            $this->db->limit($limit);
        $this->db->order_by('cmt_id', 'DESC');
        $query = $this->db->get('comment');
        return $query->result_array();
        $query->free_result();
    }

    function deletecomment($id) {
        $this->db->where('cmt_id', $id);
        $this->db->delete('comment');
    }

    function __listPage() {
        $this->db->select('*');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('page');
        return $query->result_array();
        $query->free_result();
    }

    function __detailPage($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('page');
        return $query->row_array();
        $query->free_result();
    }

    function __savePage($id) {
        $this->db->where('id', $id);
        $this->db->update('page', array(
            'html' => isset($_POST['html']) ? $_POST['html'] : NULL
        ));
    }

}
