/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    config.language = 'en';
    //config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = base_url + '/js/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = base_url + '/js/ckfinder/ckfinder.html?Type=Images';
    config.filebrowserFlashBrowseUrl = base_url + '/js/ckfinder/ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = base_url + '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = base_url + '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = base_url + '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};
