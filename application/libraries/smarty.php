<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once (APPPATH . 'libraries/libs/Smarty.class.php');

class CI_Smarty extends Smarty {

	function __construct() {
		parent::__construct();
		$this->template_dir = APPPATH . 'views/templates';
		$this->compile_dir = APPPATH . 'cache';
//		$this->caching = false;
//		$this->force_compile = true;
	}

	function view($file, $data) {
		if (is_array($data)) {
			foreach ($data as $key => $val)
				$this->assign($key, $val);
		}
		$this->display($file . '.phtml');
	}

}

?>