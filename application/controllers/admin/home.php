<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of home
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Home extends CI_Controller {
	public $data;

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Administrator";
        $this->load->view($this->view, $this->data);
    }

}
