<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Group extends CI_Controller {

    public $per_page = 20;
    public $num_links = 4;
    public $uri_segment = 4;
	public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('groups');
        $this->load->model('gmail');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index() {
        $groups = $this->groups->__getGroups();
        $config['total_rows'] = $this->groups->__totalGroups();
        $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
        $config['uri_segment'] = $this->uri_segment;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['config'] = $config;
        $this->data['view'] = 'admin/group/';
        $this->data['title'] = 'Quản lý nhóm';
        $this->data['groups'] = $groups;
        if ($_POST && $_POST['action'] == 'delete') {
            foreach ($_POST['ID'] as $id) {
                if ($_POST['chk_' . $id] == 'on')
                    $this->groups->__deleteGroups($id);
            }
            redirect('admin/group');
        }
        $this->load->view($this->view, $this->data);
    }

    public function delete($id) {
        $this->groups->__deleteGroups($id);
        redirect('admin/group');
    }

    public function edit($id) {
        $group = $this->groups->__detailGroups($id);
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['view'] = 'admin/group/edit/';
        $this->data['title'] = 'Chỉnh sửa nhóm';
        $this->data['group'] = $group;
        if ($_POST) {
            $this->groups->__updateGroups($_POST['ID']);
            redirect('admin/group');
        }
        $this->load->view($this->view, $this->data);
    }

    public function add() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Thêm nhóm";
        $this->data['view'] = 'admin/group/add/';
        if ($_POST && $this->input->get_post('name')) {
            $groupId = $this->groups->__saveGroups();
            redirect('admin/group');
        }
        $this->load->view($this->view, $this->data);
    }

    public function disable($id) {
        //Update status
        if ($this->groups->__detailGroups($id)) {
            //Delete
            $this->groups->__updateObjects($id, 0);
        }
        redirect('admin/object');
    }

    public function enable($id) {
        //Update status
        if ($this->groups->__detailGroups($id)) {
            //Delete
            $this->groups->__updateObjects($id, 1);
        }
        redirect('admin/object');
    }

}
