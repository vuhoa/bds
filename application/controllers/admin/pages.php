<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pages extends CI_Controller {

	public $controller;
	public $action;
	public $view = 'backend/admin';
	public $per_page = 20;
	public $num_links = 4;
	public $uri_segment = 4;
	public $page = 0;
	public $levels = array(
		'0' => 'Đơn cấp',
		'1' => 'Đa cấp'
	);
	public $data;

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->helper('ckeditor');
		$this->load->library('javascript');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('adm');
		$this->load->model('settings');
		$this->controller = $this->router->fetch_class();
		$this->action = $this->router->fetch_method();
		$this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
		if (!$this->session->userdata('user_id')) {
			redirect('admin');
		}
	}

	public function index() {
		$this->data['title'] = "Pages";
		$this->data['pages'] = $this->adm->__listPage();
		$this->load->view($this->view, $this->data);
	}

	public function edit($id) {
		if ($_POST) {
			$this->adm->__savePage($id);
		}
		$detail = $this->adm->__detailPage($id);
		$this->data['detail'] = $detail;
		$this->data['title'] = "Pages " . $detail['label'];
		$this->load->view($this->view, $this->data);
	}

}
