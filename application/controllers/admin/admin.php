<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of admin
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Admin extends CI_Controller {

    public $controller;
    public $action;
    public $view = 'backend/admin';
    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->data['menu'] = $this->config->item('menu');
    }

    function index() {
        if (!$this->session->userdata('user_id')) {
            redirect('admin/login', 'refresh');
        }
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Dashboard";
        $this->load->view($this->view, $this->data);
    }

    function login() {
        $this->data['title'] = "Login";
        if ($this->input->post()) {
            //Check login
            $user = $this->adm->getUser($this->input->post('username'), $this->input->post('password'));
            if ($user) {
                //update time
                $this->db->set('last_login_time', 'NOW()', FALSE);
                $this->db->where('id', $user['id']);
                $this->db->update('user');

                $this->session->set_userdata('username', $user['username']);
                $this->session->set_userdata('password', $user['password']);
                $this->session->set_userdata('user_id', $user['id']);
                $this->session->set_userdata('role_id', $user['role_id']);
                $this->session->set_userdata('fullname', $user['fullname']);
                $this->session->set_userdata('email', $user['email']);
                $this->session->set_userdata('lang', 'vi');
                redirect('admin');
            }
        }

        $this->load->view('backend/admin/login', $this->data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('admin');
    }

    function order() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Order";
        $this->data['order'] = $this->adm->__order(0, 10);
        $this->load->view($this->view, $this->data);
    }

    function comment() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Comment";
        $this->data['comment'] = $this->adm->__comment(0, 10);
        $this->load->view($this->view, $this->data);
    }

    function deletecomment($cmt_id) {
        $this->db->where('cmt_id', $cmt_id);
        $this->db->delete('comment');
        redirect('admin/comment');
    }

    function home() {
        $this->data['title'] = "Administrator";
        $this->data['lang'] = $this->session->userdata('lang');
        $this->load->view($this->view, $this->data);
    }

}
