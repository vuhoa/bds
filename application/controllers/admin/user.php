<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 26, 2015
 */
class User extends CI_Controller {

    public $per_page = 20;
    public $num_links = 4;
    public $uri_segment = 4;
	public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('users');
        $this->load->model('gmail');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index() {
        $users = $this->users->__getUsers();
//        print_r(users);die();
        $config['total_rows'] = $this->users->__totalUsers();
        $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
        $config['uri_segment'] = $this->uri_segment;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['config'] = $config;
        $this->data['view'] = 'admin/user/';
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = 'Quản lý tài khoản';
        $this->data['users'] = $users;
        if ($_POST && $_POST['action'] == 'delete') {
            foreach ($_POST['ID'] as $id) {
                if ($_POST['chk_' . $id] == 'on')
                    $this->users->__deleteUsers($id);
            }
            redirect('admin/user');
        }
        $this->load->view($this->view, $this->data);
    }

    public function delete($id) {
        $this->users->__deleteUsers($id);
        redirect('admin/contact');
    }

    public function edit($id) {
        $user = $this->users->__detailUsers($id);
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['view'] = 'admin/user/edit/';
        $this->data['title'] = 'Chỉnh sửa thông tin tài khoản';
        $this->data['user'] = $user;
        if ($_POST) {
            $this->users->__editUsers($_POST['ID']);
            redirect('admin/user');
        }
        $this->load->view($this->view, $this->data);
    }
    
    public function add() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Thêm Tài Khoản";
        $this->data['view'] = 'admin/user/add/';
        if ($_POST && $this->input->get_post('username')) {
                $userId = $this->users->__saveUsers();
                redirect('admin/user');
        }
        $this->load->view($this->view, $this->data);
    }

}
