<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of field
 * @author trungthuc
 * @date Jan 27, 2015
 */
class Field extends CI_Controller {

    public $controller;
    public $action;
    public $view = 'backend/admin';
    public $per_page = 50;
    public $num_links = 4;
    public $uri_segment = 5;
    public $page = 0;
    public $data;
    public $type = array(
        0 => 'Chọn kiểu dữ liệu',
        1 => 'INT',
        2 => 'TINYINT',
        3 => 'VARCHAR',
        4 => 'TEXT',
        5 => 'TIMESTAMP',
        6 => 'DATE',
        7 => 'DATETIME',
        8 => 'OBJECT',
        9 => 'CHECKBOX',
        10 => 'RADIO',
        11 => 'PASSWORD',
        12 => 'ALIAS',
        13 => 'SINGLE UPLOAD FILE',
        14 => 'SINGLE UPLOAD IMAGE',
        15 => 'CREATE THUMB',
        16 => 'UPLOAD MEDIA',
        17 => 'MULTI UPLOAD',
        //18 => 'CREATE MULTI THUMB',
        19 => 'TAGS',
        20 => 'JSON',
        21 => 'MULTI SELECT',
        22 => 'COMMENT',
        23 => 'MULTI OBJECT',
        24 => 'CURRENCY'
    );

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('objects');
        $this->load->model('fields');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index($object_id, $page = null) {
        $object = $this->objects->__detailObjects($object_id);
        if ($_POST && $this->input->get_post('ID')) {
            foreach ($this->input->get_post('ID') as $field_id) {
                $field = $this->fields->__detailField($field_id);
                if ($this->input->get_post('action') == 'update') {
                    //Update Ord
                    if ($this->input->get_post('ord_' . $field_id)) {
                        $this->fields->__updateOrdField($field_id, $this->input->get_post('ord_' . $field_id));
                    }
                }
                if ($this->input->get_post('action') == 'delete') {
                    //Delete
                    if ($this->input->get_post('chk_' . $field_id)) {
                        //Delete field Object
                        $this->fields->__deleteFieldObject($object['name'], $field['name']);
                        //Delete field
                        $this->fields->__deleteField($field_id);
                    }
                }
            }
            //Delete object
        }
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = $object['label'];
        $this->data['object'] = $object;
        if ($page)
            $this->page = $page;
        $config['total_rows'] = $this->fields->__totalFields($object_id);
        $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action . '/' . $object_id;
        $config['uri_segment'] = $this->uri_segment;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['config'] = $config;
        $this->data['rows'] = $this->fields->__getAllField($object_id, $this->per_page, $this->page);
        $this->load->view($this->view, $this->data);
    }

    public function disable($object_id, $id) {
        //Update status
        if ($this->fields->__detailFields($id)) {
            //Delete
            $this->fields->__updateFields($id, 0);
        }
        redirect('admin/field/index/' . $object_id);
    }

    public function enable($object_id, $id) {
        //Update status
        if ($this->fields->__detailFields($id)) {
            //Delete
            $this->fields->__updateFields($id, 1);
        }
        redirect('admin/field/index/' . $object_id);
    }

    public function delete($object_id, $field_id) {
        $object = $this->objects->__detailObjects($object_id);
        if ($this->fields->__detailFields($field_id)) {
            $field = $this->fields->__detailField($field_id);
            //Delete
            $this->fields->__deleteFieldObject($object['name'], $field['name']);
            $this->fields->__deleteField($field_id);
        }
        redirect('admin/field/index/' . $object_id);
    }

    function add($object_id) {
        $object = $this->objects->__detailObjects($object_id);
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = $object['label'];
        $this->data['object'] = $object;
        $this->data['type'] = $this->type;
        if ($_POST && $this->input->get_post('name')) {
            //Kiem tra field nay da co trong table chua
            if (!$this->db->field_exists($this->input->get_post('name'), $object['name'])) {
                //Tao field tren data.
                $this->fields->__addField($object['name']);
                // Save field
                $this->fields->__saveField($object_id);
                redirect('admin/field/index/' . $object_id);
            }
        }
        $this->load->view($this->view, $this->data);
    }

    function edit($object_id, $field_id) {
        $object = $this->objects->__detailObjects($object_id);
        $fields = $this->fields->__getFieldDetail($field_id);
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = $fields['label'];
        $this->data['object'] = $object;
        $this->data['field'] = $fields;
        $this->data['type'] = $this->type;
        if (isset($_POST) && $this->input->get_post('name')) {
            //Sua field tren data.
            $this->fields->__editField($object['name'], $fields, $field_id);
            // Update field
            $this->fields->__updateField($object_id, $fields, $field_id);
            redirect('admin/field/index/' . $object_id);
        }

        $this->load->view($this->view, $this->data);
    }

}
