<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('settings');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index() {
        $setting = $this->settings->__getSettings();
        $data['lang'] = $this->session->userdata('lang');
        $data['view'] = 'admin/setting/edit/';
        $data['title'] = 'Cấu hình hệ thống';
        $data['settings'] = $setting;
        //print_r($_POST);die('f');
        if ($_POST) {
            $this->settings->__editSetting();
            redirect('admin/setting');
        }
        $this->load->view($this->view, $data);
    }

}
