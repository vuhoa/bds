<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Description of error
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Error extends CI_Controller {

	public $controller;
	public $action;
	public $layout = 'layout';
	public $per_page = 15;
	public $num_links = 4;
	public $uri_segment = 5;
	public $page = 0;
	public $data;

	public function __construct() {
		parent::__construct();
		$this->load->library('smarty');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->model('b');
		$this->load->library('pagination');
		$this->load->helper('form');
		$this->config->load('conf');
		$this->controller = $this->router->fetch_class();
		$this->action = $this->router->fetch_method();
		$this->data['view'] = $this->router->fetch_method();
		$this->data['content'] = $this->router->fetch_class() . '/' . $this->router->fetch_method();

		$this->data['meta_description'] = $this->config->item('descriptions');
		$this->data['meta_keyword'] = $this->config->item('keywords');
		$this->data['meta_title'] = $this->config->item('title');

		$this->data['ext'] = $this->b->__get_extension();
		$this->data['menu_top'] = $this->b->__menu_top();
		$this->data['menu_top_mobile'] = $this->b->__menu_top_nobile();
		$this->data['style'] = $this->b->__styleHome();
		$this->data['viewest'] = $this->b->__postViewest(10);
		$this->data['menu_right'] = $this->b->__menu_right();
	}

	function index() {
            
		$this->data['meta_title'] = 'Trang bạn tìm kiếm không tồn tại | AIE Home';
		$this->smarty->view($this->layout, $this->data);
	}

}
