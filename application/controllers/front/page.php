<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller {

    public $controller;
    public $action;
    public $layout = 'layout';
    public $per_page = 15;
    public $num_links = 4;
    public $uri_segment = 5;
    public $page = 0;
    public $data;
    public $cfg;

    public function __construct() {

        parent::__construct();
        $this->load->library('smarty');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model('b');
        $this->load->library('pagination');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('text');
        $this->load->config('conf');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->data['view'] = $this->router->fetch_method();
        $this->data['content'] = $this->router->fetch_class() . '/' . $this->router->fetch_method();
        $this->data['cfg'] = $this->cfg = (Object) $this->config->config;

        $this->data['meta_description'] = $this->cfg->descriptions;
        $this->data['meta_keyword'] = $this->cfg->keywords;
        $this->data['meta_title'] = $this->cfg->title;

        $this->data['ext'] = $this->b->__get_extension();
//        print_r($this->data['ext']['company']);
//        die();
        $this->data['menu_tops'] = $this->b->__menuTops();

        $this->data['menu'] = $this->b->__menu();
        $this->data['category'] = $this->b->__category();
        $this->data['category_news'] = $this->b->__categoryNews();
        $this->data['slider'] = $this->b->__slider();
        $this->data['supports'] = $this->b->__support();
        $this->data['video'] = $this->b->__video();
        $this->data['partners'] = $this->b->__partners();
        $this->data['hotnews'] = $this->b->__getHotNews();


        $this->data['this'] = $this;
    }

    function contact() {
        $alias = $this->uri->segment(1);
        $this->data['content_menu'] = $this->b->__menuContent($alias);
        $this->data['meta_title'] = $this->data['content_menu']['meta_title'];
        $this->data['meta_description'] = $this->data['content_menu']['meta_description'];
        $this->data['meta_keyword'] = $this->data['content_menu']['meta_keywords'];
        $this->data['save_success'] = '';
        if (isset($_POST) && $_POST) {
            $data = [
                'name'=>$_POST['name'],
                'username'=>$_POST['username'],
                'address'=>$_POST['address'],
                'phone'=>$_POST['phone'],
                'email'=>$_POST['email'],
                'subject'=>$_POST['subject'],
                'content'=>strip_tags($_POST['content'],'<span>')
            ];

            //print_r($_POST);
            //die();
            //$site_key_post = $_POST['g-recaptcha-response'];
            //if (!empty($site_key_post)) {
            //lấy dữ liệu được post lên
//            $body = "Thông tin khách gửi yêu cầu <br />";
//            $body .= "------------------------------------------------------</br>";
//            $body .= "<p>Khách hàng: <strong>" . $_POST['name'] . "</strong></p>";
//            $body .= "<p>Danh xưng: <strong>" . $_POST['username'] . "</strong></p>";
//            $body .= "<p>Email: " . $_POST['email'] . "</p>";
//            $body .= "<p>Điện thoại: " . $_POST['phone'] . "</p>";
//            $body .= "<p>Địa chỉ: " . $_POST['address'] . "</p>";
//            $body .= $_POST['content'];
//            $this->gmail->send_mail('contact@bluesoft.vn', $_POST['email'], $_POST['name'], $_POST["subject"], $body);
//            $body1 = "<h4>Cám ơn <strong>" . $_POST['name'] . "</strong> đã gửi liên hệ cho ".$this->data['ext']['company']['content']."</h4></br>";
//            $body1 .= "Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.</br>";
//            $body1 .= "<p>---------------------------------------------------------------------------------<p>";
//            $body1 .= "<p><strong>".$this->data['ext']['company']."</strong></p>";
//            $body1 .= "<p>Điện thoại: <strong>".$this->data['ext']['hotline']['content']."</strong></p>";
//            $body1 .= "<p>Địa chỉ: Tầng 5 số 174 Nguyễn Tuân, Thanh Xuân, Hà Nội.</p>";
//            $body1 .= "<p>---------------------------------------------------------------------------------</p>";
//            $this->gmail->send_mail($this->data['ext']['email']['content'], $this->data['ext']['email']['content'], $this->data['ext']['company']['content'], $_POST["subject"], $body1);
            $this->b->__save_contact($data,$this->session->userdata('role_id'));
            if ($this->b->__save_contact($data,$this->session->userdata('role_id'))) {
                $this->data['save_success'] = "Success: Your company information was successfully uploaded. We will revise and notice you when your information is public on our system. Thanks for uploading.";
            } else {
                $this->data['save_success'] = "Error!!!";
            }
            //} else {
            //$this->data['captcha_google'] = 'Captcha error';
            //$this->Bluesoft_m->__save_post_listing($_post);
            //}
        }

        $this->smarty->view($this->layout, $this->data);
    }

    function tinhnang() {
        $this->data['view'] = 'tinh-nang';
        $this->data['meta_title'] = 'Tính năng';
        $this->smarty->view($this->layout, $this->data);
    }

    function banggia() {
        $this->data['view'] = 'bang-gia';
        $this->data['meta_title'] = 'Bảng giá';
        $this->smarty->view($this->layout, $this->data);
    }

    function khogiaodien() {
        $this->data['view'] = 'kho-giao-dien';
        $this->data['meta_title'] = 'Kho giao diện';
        $this->smarty->view($this->layout, $this->data);
    }

    function about() {
        $alias = $this->uri->segment(1);
        $this->data['content_menu'] = $this->b->__menuContent($alias);
        $this->data['meta_title'] = $this->data['content_menu']['meta_title'];
        $this->data['meta_description'] = $this->data['content_menu']['meta_description'];
        $this->data['meta_keyword'] = $this->data['content_menu']['meta_keywords'];
        $this->smarty->view($this->layout, $this->data);
    }

}
