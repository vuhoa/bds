<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public $controller;
    public $action;
    public $layout = 'layout';
    public $newD = 'detail_news';
    public $per_page = 15;
    public $num_links = 4;
    public $uri_segment = 5;
    public $page = 0;
    public $data;
    public $cfg;

    public function __construct() {

        parent::__construct();
        $this->load->library('smarty');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model('b');
        $this->load->library('pagination');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('text');
        $this->load->config('conf');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->data['view'] = $this->router->fetch_method();
        $this->data['content'] = $this->router->fetch_class() . '/' . $this->router->fetch_method();
        $this->data['cfg'] = $this->cfg = (Object) $this->config->config;
        if (!$this->session->userdata('language')) {
            $this->session->set_userdata('language', 1);
        }
        $this->language = $this->session->userdata('language');
        $this->data['language'] = $this->language;

        $this->data['meta_description'] = $this->cfg->descriptions;
        $this->data['meta_keyword'] = $this->cfg->keywords;
        $this->data['meta_title'] = $this->cfg->title;

        $this->data['ext'] = $this->b->__get_extension();
//        $this->data['menu_tops'] = $this->b->__menuTops();
//
        $this->data['menu'] = $this->b->__menu(0);
//        $this->data['category'] = $this->b->__category();
//        $this->data['category_news'] = $this->b->__categoryNews();
//        $this->data['slider'] = $this->b->__slider();
//        $this->data['supports'] = $this->b->__support();
//        $this->data['video'] = $this->b->__video();
//        $this->data['partners'] = $this->b->__partners();
//        $this->data['tieubieu'] = $this->b->__typeProducts(1);
//        $this->data['banchay'] = $this->b->__typeProducts(2);
//        $this->data['hotnews'] = $this->b->__getHotNews();
//        print_r($this->data['ext']['company']['link_left']);
//        die();
        //print_r($this->session->userdata('customer_phone'));die();
        date_default_timezone_set("Asia/Bangkok");
        $this->data['date_now'] = date('Y-m-d H:i:s');
        $this->data['seo'] = $this->b->__getSEO();
        $this->data['social'] = $this->b->__getSocial();
        $this->data['alias'] = 'pages';
        $this->data['this'] = $this;
    }

    function index() {
        $this->data['alias'] = 'home';
        $this->data['section1'] = $this->b->__menuContent('trang-chu');
        $this->data['section2'] = $this->b->__menuContent('lien-he');
        $this->data['section3'] = $this->b->__menuContent('gioi-thieu');
        $this->data['section4'] = $this->b->__menuContent('vi-tri-du-an');
        $this->data['section5'] = $this->b->__menuContent('tien-ich');
        $this->data['section6'] = $this->b->__menuContent('mat-bang-can-ho');
        $this->data['homeDocs'] = $this->b->_getHomeDocs(1);
        $this->data['homeImage'] = $this->b->_getHomeDocs(2);
        $this->data['homeVideo'] = $this->b->_getHomeVideo();
        $this->data['meta_description'] = $this->data['section1']['meta_description'];
        $this->data['meta_keyword'] = $this->data['section1']['meta_keywords'];
        $this->data['meta_title'] = $this->data['section1']['meta_title'];
        $this->data['meta_image'] = $this->data['section1']['img_share'];
        $this->smarty->view($this->layout, $this->data);
    }

    function about() {
        $this->data['aboutCate'] = $this->b->__menu(24);
        $this->data['section3'] = $this->b->__menuContent('gioi-thieu');
        $this->data['meta_description'] = $this->data['section3']['meta_description'];
        $this->data['meta_keyword'] = $this->data['section3']['meta_keywords'];
        $this->data['meta_title'] = $this->data['section3']['meta_title'];
        $this->data['meta_image'] = $this->data['section3']['img_share'];
        $this->smarty->view($this->layout, $this->data);
    }

    function location() {
        $this->data['location'] = $this->b->__menuContent('vi-tri-du-an');
        $this->data['meta_description'] = $this->data['location']['meta_description'];
        $this->data['meta_keyword'] = $this->data['location']['meta_keywords'];
        $this->data['meta_title'] = $this->data['location']['meta_title'];
        $this->data['meta_image'] = $this->data['location']['img_share'];
        $this->smarty->view($this->layout, $this->data);
    }

    function ground() {
        $this->data['section6'] = $this->b->__menuContent('mat-bang-can-ho');
        $this->data['meta_description'] = $this->data['section6']['meta_description'];
        $this->data['meta_keyword'] = $this->data['section6']['meta_keywords'];
        $this->data['meta_title'] = $this->data['section6']['meta_title'];
        $this->data['meta_image'] = $this->data['section6']['img_share'];
        $this->data['ground'] = $this->b->__menu(27);
        $this->smarty->view($this->layout, $this->data);
    }

    function library() {
        $this->data['section7'] = $this->b->__menuContent('thu-vien-tai-lieu');
        $this->data['meta_description'] = $this->data['section7']['meta_description'];
        $this->data['meta_keyword'] = $this->data['section7']['meta_keywords'];
        $this->data['meta_title'] = $this->data['section7']['meta_title'];
        $this->data['meta_image'] = $this->data['section7']['img_share'];
        $this->data['libraries'] = $this->b->__menu(29);
        $this->data['libCategory'] = $this->b->__libCategory();
        $this->data['videos'] = $this->b->__video();
        $this->smarty->view($this->layout, $this->data);
    }

    function apartment() {
        $homeImage = $this->b->_getHomeDocs(2);
        $this->data['homeImage'] = json_decode($homeImage['image'],true);
        $this->smarty->view($this->layout, $this->data);
    }

    function sample() {
        $num = $_GET['num'];
        $homeImage = $this->b->_getUtilityDetail($num);
        $this->data['homeImage'] = json_decode($homeImage['image'],true);
        $this->smarty->view($this->layout, $this->data);
    }

    function lib() {
        $num = $_GET['num'];
        $homeImage = $this->b->_getLibraryDetail($num);
        $this->data['homeImage'] = json_decode($homeImage['image'],true);
        $this->smarty->view($this->layout, $this->data);
    }

    function docs() {
        $homeDocs = $this->b->_getHomeDocs(1);
        $this->data['homeDocs'] = json_decode($homeDocs['image'],true);
        $this->smarty->view($this->layout, $this->data);
    }

    function xacnhan() {
        $phone =  $this->uri->segment(2);
        $price =  $this->uri->segment(3);
        $id =  $this->uri->segment(4);
        $this->db->set('status',1);
        $this->db->where('id', $id);
        $this->db->update('lienhe');
        $tk = $this->b->getAccount($phone);
        if ($tk) {
            $total = (int) $price + (int) $tk['total'];
            $this->db->set('total',$total);
            $this->db->where('phone', $phone);
            $this->db->update('links');
        } else {
            $data_acc = [
                'name'=>$phone,
                'phone'=>$phone,
                'total'=>$price,
            ];
            $this->b->__save_account($data_acc,5);
        }
        $link = base_url().'admin/record/index/13';
        redirect($link);
    }

    function video() {
        if (!isset($_GET['num']))
            $this->data['homeVideo'] = $this->b->_getHomeVideo();
        else
            $this->data['homeVideo'] = $this->b->_getVideoDetail($_GET['num']);
        $this->smarty->view($this->layout, $this->data);
    }

    function news() {
        $this->data['newsContent'] = $this->b->__menuContent('tin-tuc-su-kien');
        $this->data['meta_description'] = $this->data['newsContent']['meta_description'];
        $this->data['meta_keyword'] = $this->data['newsContent']['meta_keywords'];
        $this->data['meta_title'] = $this->data['newsContent']['meta_title'];
        $this->data['meta_image'] = $this->data['newsContent']['img_share'];
        $this->data['news'] = $this->b->__getNews();
        $this->smarty->view($this->layout, $this->data);
    }
    function tiendo() {
        $this->data['newsContent'] = $this->b->__menuContent('tin-tuc-su-kien');
        $this->data['meta_description'] = $this->data['newsContent']['meta_description'];
        $this->data['meta_keyword'] = $this->data['newsContent']['meta_keywords'];
        $this->data['meta_title'] = $this->data['newsContent']['meta_title'];
        $this->data['meta_image'] = $this->data['newsContent']['img_share'];
        $this->data['news'] = $this->b->__getNews();
        $this->smarty->view($this->layout, $this->data);
    }

    function detail_news() {
        $alias = $this->uri->segment(1);
        $this->data['newDetail'] = $this->b->__detailNews($alias);
        $this->data['meta_description'] = $this->data['newDetail']['meta_descriptions'];
        $this->data['meta_keyword'] = $this->data['newDetail']['meta_keyword'];
        $this->data['meta_title'] = $this->data['newDetail']['meta_title'];
        $this->data['meta_image'] = $this->data['newDetail']['img'];
        $this->data['newsContent'] = $this->b->__menuContent('tin-tuc-su-kien');
        $this->data['news'] = $this->b->__getNews();
        //$this->data['newDetailBK'] = $this->b->__getNewsOne();
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            $this->smarty->view($this->newD,$this->data);
        } else {
            $this->smarty->view($this->layout, $this->data);
        }
    }

    function utility() {
        $this->data['utilityContent'] = $this->b->__menuContent('tien-ich');
        $this->data['meta_description'] = $this->data['utilityContent']['meta_description'];
        $this->data['meta_keyword'] = $this->data['utilityContent']['meta_keywords'];
        $this->data['meta_title'] = $this->data['utilityContent']['meta_title'];
        $this->data['meta_image'] = $this->data['utilityContent']['img_share'];
        $this->data['utilities'] = $this->b->__getUtility();
        $this->smarty->view($this->layout, $this->data);
    }

    function contact() {
        if (isset($_POST) && $_POST) {
            $data = [
                'name'=>$_POST['username'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
            ];
            if ($this->b->__save_contact($data,1)) {
                echo '<script language="javascript">';
                echo 'alert("Đăng ký nhận thông tin dự án thành công")';
                echo '</script>';
                redirect(base_url());
            } else {
                echo '<script language="javascript">';
                echo 'alert("Đăng ký nhận thông tin dự án thất bại")';
                echo '</script>';
                redirect(base_url('lien-he').'.html');
            }


//            if ($this->b->__save_contact($data,1)) {
//                $config = Array(
//                    'charset' => 'utf-8',
//                    'protocol' => 'smtp',
//                    'smtp_host' => 'ssl://smtp.googlemail.com',
//                    'smtp_port' => 465,
//                    'smtp_user' => $this->data['ext']['company']['link_left'], // change it to yours
//                    'smtp_pass' => $this->data['ext']['company']['link_rightt'], // change it to yours
//                    'mailtype' => 'html',
//                    'wordwrap' => TRUE
//                );
//
//                $body = "Thông tin gửi yêu cầu <br />";
//                $body .= "------------------------------------------------------</br>";
//                $body .= "<p>Tên: <strong>" . $_POST['name'] . "</strong></p>";
//                $body .= "<p>Email: <strong>" . $_POST['email'] . "</strong></p>";
//                $body .= "<p>Điện thoại: <strong>" . $_POST['phone'] . "</strong></p>";
//                $body .= "<p>Năm sinh: <strong>" . $_POST['date']. "</strong></p>";
//                $body .= "<p>Trường học : <strong>" . $university. "</strong></p>";
//                $body .= "<p>Chuyên ngành: <strong>" . $major. "</strong></p>";
//                $body .= "<p>Điểm môn 1: <strong>" . $subject1. "</strong></p>";
//                $body .= "<p>Điểm môn 2: <strong>" . $subject2. "</strong></p>";
//                $body .= "<p>Điểm môn 3: <strong>" . $subject3. "</strong></p>";
//                $body .= "<p>Điểm môn toán: <strong>" . $math. "</strong></p>";
//                $body .= "<p>Điểm môn văn: <strong>" . $literature. "</strong></p>";
//                $body .= "<p>Điểm môn ngoại ngữ: <strong>" . $language. "</strong></p>";
//                $body .= "<p>Môn tự chọn 1: <strong>" . $subject_option. "</strong></p>";
//                $body .= "<p>Điểm môn tự chọn 1: <strong>" . $subject_option_score. "</strong></p>";
//                $body .= "<p>Môn tự chọn 2: <strong>" . $subject_option2. "</strong></p>";
//                $body .= "<p>Điểm môn tự chọn 2: <strong>" . $subject_option_score2. "</strong></p>";
//                $body .= "<p>Môn tự chọn 3: <strong>" . $subject_option3. "</strong></p>";
//                $body .= "<p>Điểm môn tự chọn 3: <strong>" . $subject_option_score3. "</strong></p>";
//
//                $this->load->library('email', $config);
//                $this->email->set_newline("\r\n");
//                $this->email->from($_POST['email']); // change it to yours
//                $this->email->to($this->data['ext']['company']['link_left']);// change it to yours
//                $this->email->subject('Email đăng ký tư vấn');
//                $this->email->message($body);
//                $this->email->send();
//                echo '<script language="javascript">';
//                echo 'alert("Yêu cầu tư vấn thành công. Chúng tôi sẽ liên lạc lại với bạn trong thời gian sớm nhất. Chúc bạn 1 ngày vui vẻ!")';
//                echo '</script>';
//            } else {
//                echo '<script language="javascript">';
//                echo 'alert("Mail đăng ký không hợp lệ")';
//                echo '</script>';
//            }

        } else {
            $this->data['contact'] = $this->b->__menuContent('lien-he');
            $this->smarty->view($this->layout, $this->data);
        }
    }

}
