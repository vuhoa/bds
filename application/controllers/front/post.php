<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Post extends CI_Controller {

    public $controller;
    public $action;
    public $layout = 'layout';
    public $per_page = 15;
    public $num_links = 4;
    public $uri_segment = 5;
    public $page = 0;
    public $data;

    public function __construct() {

        parent::__construct();
        $this->load->library('smarty');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model('b');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('text');
        $this->load->config('conf');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->data['view'] = $this->router->fetch_method();
        $this->data['content'] = $this->router->fetch_class() . '/' . $this->router->fetch_method();
        $this->data['cfg'] = $this->cfg = (Object) $this->config->config;

        $this->data['meta_description'] = $this->cfg->descriptions;
        $this->data['meta_keyword'] = $this->cfg->keywords;
        $this->data['meta_title'] = $this->cfg->title;

        $this->data['ext'] = $this->b->__get_extension();
        $this->data['menu_tops'] = $this->b->__menuTops();

        $this->data['menu'] = $this->b->__menu();
        $this->data['category'] = $this->b->__category();
        $this->data['category_news'] = $this->b->__categoryNews();
        $this->data['slider'] = $this->b->__slider();
        $this->data['supports'] = $this->b->__support();
        $this->data['video'] = $this->b->__video();
        $this->data['partners'] = $this->b->__partners();
        $this->data['tieubieu'] = $this->b->__typeProducts(1);
        $this->data['banchay'] = $this->b->__typeProducts(2);
        $this->data['hotnews'] = $this->b->__getHotNews();

        $this->data['this'] = $this;
    }

    function index($page = 1) {
        $alias = $this->uri->segment(1);
        $this->data['content_menu'] = $this->b->__menuContent($alias);
        $this->data['meta_title'] = $this->data['content_menu']['meta_title'];
        $this->data['meta_description'] = $this->data['content_menu']['meta_description'];
        $this->data['meta_keyword'] = $this->data['content_menu']['meta_keywords'];
        $this->per_page = 24;
        $config['total_rows'] = count($this->b->__getNews(null, null));
        $config['base_url'] = base_url() . $alias;
        $config['uri_segment'] = 2;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        if ($this->uri->segment(2) != '') {
            $page = $this->uri->segment(2);
            $start = ($page - 1) * $this->per_page;
        } else {
            $start = 0;
        }
//        print_r($start);
//        die();
        $config['use_page_numbers'] = TRUE;
        $this->data['config'] = $config;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['rows'] = $this->b->__getNews($config['per_page'], $start);
        $this->smarty->view($this->layout, $this->data);
    }

    function news_cate($page = 1) {
        $alias = $this->uri->segment(1);
        $this->data['content_menu'] = $this->b->__categoryNewsContent($alias);
        $this->data['meta_title'] = $this->data['content_menu']['meta_title'];
        $this->data['meta_description'] = $this->data['content_menu']['meta_description'];
        $this->data['meta_keyword'] = $this->data['content_menu']['meta_keywords'];
        $this->per_page = 24;
        $config['total_rows'] = count($this->b->__getNewsByCate(null, null,$this->data['content_menu']['id']));
        $config['base_url'] = base_url() . $alias;
        $config['uri_segment'] = 2;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        if ($this->uri->segment(2) != '') {
            $page = $this->uri->segment(2);
            $start = ($page - 1) * $this->per_page;
        } else {
            $start = 0;
        }
//        print_r($start);
//        die();
        $config['use_page_numbers'] = TRUE;
        $this->data['config'] = $config;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['rows'] = $this->b->__getNewsByCate($config['per_page'], $start,$this->data['content_menu']['id']);
        $this->smarty->view($this->layout, $this->data);
    }

    function detail_news() {
        $alias = $this->uri->segment(1);
        $this->data['new'] = $this->b->__detailNews($alias);
//        print_r($this->data['product_file']);
//        die();
        $this->data['meta_title'] = $this->data['new']['meta_title'];
        $this->data['meta_description'] = $this->data['new']['meta_descriptions'];
        $this->data['meta_keyword'] = $this->data['new']['meta_keyword'];

        $this->smarty->view($this->layout, $this->data);
    }

}
