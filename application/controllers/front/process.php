<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Process extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->library('minify');
    }

    function dichvu($slug) {
        $redirect = base_url($slug . $this->config->item('url_suffix'));
        redirect($redirect);
        exit();
    }

    function kienthuc() {
        $redirect = base_url('tu-van' . $this->config->item('url_suffix'));
        redirect($redirect);
        exit();
    }

    function dkienthuc($slug) {
        $redirect = base_url('tu-van/' . $slug . $this->config->item('url_suffix'));
        redirect($redirect);
        exit();
    }

    function minifycss() {
        $this->load->library('minify');
        $files = array(
            'bootstrap.min.css',
            'jquery.mmenu.all.css',
            'styles.css',
            'responsive.css'
        );
        $this->minify->css($files);
        echo $this->minify->deploy_css(TRUE);
        echo 'CSS Done';
    }

    function minifyjs() {
        $this->load->library('minify');
        $files = array(
            'jquery.min.js',
            'bootstrap.min.js',
            'jquery.mmenu.all.min.js',
            'scripts.js'
        );
        $this->minify->js($files);
        echo $this->minify->deploy_js(TRUE);
        echo 'SCRIPT Done';
    }

    function sitemap() {
        set_time_limit(3600);
        $myString = '';
        $myString .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
        $myString .= '<url><loc>' . base_url() . '</loc><lastmod>2017-11-03T14:07:48+07:00</lastmod><changefreq>daily</changefreq><priority>1.0</priority></url>';

        $this->db->select('id, name, alias');
        $this->db->where('status', 1);
        $sql = $this->db->get('category');
        $categories = $sql->result_array();

        foreach ($categories as $category) {
            $myString .= '<url><loc>' . base_url() . $category["alias"] . '.html</loc><lastmod>2017-11-02T07:30:01+07:00</lastmod><changefreq>always</changefreq><priority>0.4</priority></url>';
        }

        $this->db->select('id, name, alias, img');
        $this->db->where('status', 1);
        $query = $this->db->get('post');
        foreach ($query->result_array() as $row) {
            $myString .= '<url><loc>' . base_url() . $row["alias"] . '.html</loc><image:image><image:loc>' . base_url() . 'images/' . $row["img"] . '</image:loc><image:license>' . base_url() . '</image:license><image:family_friendly>yes</image:family_friendly></image:image><lastmod>2017-11-03T00:02:00+07:00</lastmod><changefreq>daily</changefreq><priority>0.8</priority></url>';
        }

        $this->db->select('id, name, alias, img');
        $this->db->where('status', 1);
        $query = $this->db->get('tuvan');
        foreach ($query->result_array() as $row) {
            $myString .= '<url><loc>' . base_url() . $row["alias"] . '.html</loc><image:image><image:loc>' . base_url() . 'images/' . $row["img"] . '</image:loc><image:license>' . base_url() . '</image:license><image:family_friendly>yes</image:family_friendly></image:image><lastmod>2017-11-03T00:02:00+07:00</lastmod><changefreq>daily</changefreq><priority>0.8</priority></url>';
        }

        $this->db->select('id, name, alias, img');
        $this->db->where('status', 1);
        $query = $this->db->get('project');
        foreach ($query->result_array() as $row) {
            $myString .= '<url><loc>' . base_url() . 'du-an/' . $row["alias"] . '.html</loc><image:image><image:loc>' . base_url() . 'images/' . $row["img"] . '</image:loc><image:license>' . base_url() . '</image:license><image:family_friendly>yes</image:family_friendly></image:image><lastmod>2017-11-03T00:02:00+07:00</lastmod><changefreq>daily</changefreq><priority>0.8</priority></url>';
        }

        $myString .= '</urlset>';
        unlink('sitemap.xml');
        $fh = fopen('sitemap.xml', "w");
        chmod("sitemap.xml", 777);
        fwrite($fh, $myString);
        fclose($fh);
    }

}
