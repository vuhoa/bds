<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project extends CI_Controller {

    public $controller;
    public $action;
    public $layout = 'layout';
    public $per_page = 15;
    public $num_links = 4;
    public $uri_segment = 5;
    public $page = 0;
    public $data;

    public function __construct() {

        parent::__construct();
        $this->load->library('smarty');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model('b');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->config->load('conf');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->data['view'] = $this->router->fetch_method();
        $this->data['content'] = $this->router->fetch_class() . '/' . $this->router->fetch_method();

        $this->data['meta_description'] = $this->config->item('descriptions');
        $this->data['meta_keyword'] = $this->config->item('keywords');
        $this->data['meta_title'] = $this->config->item('title');

        $this->data['ext'] = $this->b->__get_extension();
        $this->data['menu_top'] = $this->b->__menu_top();
        $this->data['menu_top_mobile'] = $this->b->__menu_top_nobile();
        $this->data['style'] = $this->b->__styleHome();
        $this->data['viewest'] = $this->b->__postViewest(10);
        $this->data['menu_right'] = $this->b->__menu_right();
        $this->data['pojectest'] = $this->b->__projectViewest(15);
    }

    function index($page = null) {
        if ($page)
            $this->page = $page;
        $config['total_rows'] = $this->b->__totalProject();
        $config['base_url'] = base_url() . 'du-an';
        $config['uri_segment'] = 2;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['config'] = $config;
        $this->data['rows'] = $this->b->__projectByCates($this->per_page, $this->page);
        $this->data['meta_title'] = 'Dự án đã triển khai';
        $this->data['meta_title'] = ($page) ? $this->data['meta_title'] . ' | Trang ' . $page : $this->data['meta_title'];
        $this->data['this'] = $this;

        $this->smarty->view($this->layout, $this->data);
    }

    function detail($slug) {
        $detail = $this->b->__detailProject($slug);
        if ($detail) {
            $this->data['detail'] = $detail;
            $this->data['sames'] = $this->b->__postSameCate($detail['id'], $detail['cate'], 10);
            $this->data['psames'] = $this->b->__supportSameByCate($detail['cate'], 15);
            $this->data['meta_description'] = $detail['meta_descriptions'];
            $this->data['meta_keyword'] = $detail['meta_keywords'];
            $this->data['meta_title'] = ($detail['meta_title']) ? $detail['meta_title'] : $detail['name'];
            $this->data['projects'] = $this->b->__projectByCateNot($detail['id'], $detail['cate'], 20);
            //update views
            $this->b->__updateViewProject($detail['id']);
            if ($detail['parent_cate']) {
                $this->data['parent_cate'] = $this->b->__detailCategoryById($detail['parent_cate']);
            }
        } else {
            $this->data['view'] = 'error';
        }

        $this->smarty->view($this->layout, $this->data);
    }

}
