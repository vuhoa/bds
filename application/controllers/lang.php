<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of lang
 * @author trungthuc
 * @date Jul 25, 2015
 */
class Lang extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('azs');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        if (!$this->session->userdata('language')) {
            $this->session->set_userdata('language', 1);
        }
        if($this->uri->segment(3) == 'vi')
            $this->session->set_userdata('language', 1);
        else
            $this->session->set_userdata('language', 2);
    }

    public function index($lang) {
        if (!$this->session->userdata('language')) {
            $this->session->set_userdata('language', 1);
        }
        if($this->uri->segment(3) == 'vi')
            $this->session->set_userdata('language', 1);
        else
            $this->session->set_userdata('language', 2);
        redirect(base_url());
        exit();
    }

}
