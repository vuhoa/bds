<?php

/**
 * Description of niceditor_helper
 * @author trungthuc
 * @date Feb 3, 2015
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function form_niceditor($data) {

    $html = script_tag('template/backend/js/nicEdit.js');
    $html .= '<script type="text/javascript">'
            . 'bkLib.onDomLoaded(function() {'
            . 'new nicEditor().panelInstance("' . $data['name'] . '");'
            . '});'
            . '</script>';
    $html .= form_textarea(array(
        'name' => $data['name'],
        'id' => $data['name'],
        'width' => $data['width'],
        'height' => $data['height'],
        'value' => $data['value']
    ));
    return $html;
}
