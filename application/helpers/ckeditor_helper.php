<?php

/**
 * Description of fckeditor_helper
 * @author trungthuc
 * @date Feb 3, 2015
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function form_ckeditor($data) {
    $data['language'] = isset($data['language']) ? $data['language'] : 'en';
    $size = isset($data['width']) ? 'width: "' . $data['width'] . '", ' : '100%';
    $size .= isset($data['height']) ? 'height: "' . $data['height'] . '", ' : '';

    $options = '{' .
            $size .
            'language: "' . $data['language'] . '", 
            stylesCombo_stylesSet: "my_styles",
            //startupOutlineBlocks: true,
            entities: false,
            entities_latin: false,
            entities_greek: false,
            forcePasteAsPlainText: false,
            
            
            filebrowserImageUploadUrl : "' . base_url() . 'template/backend/ckfinder/core/connector/php/connector.php?command=QuickUpload", 
            filebrowserImageBrowseUrl : "' . base_url() . 'template/backend/ckfinder/ckfinder.html", 
            filebrowserUploadUrl: "' . base_url() . 'template/backend//ckfinder/core/connector/php/connector.php?command=QuickUpload", 
            filebrowserBrowseUrl: "' . base_url() . 'template/backend/ckfinder/ckfinder.html", 
            filebrowserImageWindowWidth : "80%",
            filebrowserImageWindowHeight : "80%",   

            
            toolbar :[
                ["Source","-","FitWindow","ShowBlocks","-","Preview"],
                ["Undo","Redo","-","Find","Replace","-","SelectAll","RemoveFormat"],
                ["Cut","Copy","Paste","PasteText","PasteWord","-","Print","SpellCheck"],
                ["Form","Checkbox","Radio","TextField","Textarea","Select","Button","ImageButton","HiddenField"],  
                "/",
                ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"],
                ["Bold","Italic","Underline", "Strike", "-", "Subscript", "Superscript", "Colors", "Font", "FontSize", "Format"],
				["TextColor","BGColor"],
				["HorizontalRule"],
                ["OrderedList","UnorderedList","-","Blockquote","CreateDiv"],
                ["Image","Flash","Table", "Smiley"],
                ["Link","Unlink","Anchor"],
                ["Rule","SpecialChar", "PageBreak", "Iframe"]
            ]
        }';

    $html = form_textarea(array(
        'name' => $data['name'],
        'id' => $data['name'],
        'class' => 'ckeditor',
        'width' => $data['width'],
        'height' => $data['height'],
        'value' => $data['value']
    ));
    $html .= script_tag('template/backend/ckeditor/ckeditor.js');
    $html .= script_tag('template/backend/ckeditor/adapters/jquery.js');
    $html .= '<script type="text/javascript">CKEDITOR.replace("' . $data['id'] . '", ' . $options . ');</script>';
    return $html;
}
