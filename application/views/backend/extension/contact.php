<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open('', array('class' => 'objects')); ?>
        <button type="button" id="btn_delete" class="btn btn-danger btn-rounded"><i class="iconfa-remove"></i> Xóa</button>
		<br />

        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">#ID</th>
                <th class="head0">Người liên hệ</th>
				<th class="head1">Điện thoại</th>
				<th class="head0">Địa chỉ</th>
				<th class="head1">Tiêu đề</th>
				<th class="head0">Nội dung</th>
                <th class="head1">Ngày gửi</th>
				<th class="head0">Xóa</th>
            </tr>

			<?php foreach ($rows as $row): ?>
				<tr class="gradeX">
					<td class="aligncenter" width="10">
						<span class="center">
							<input type="checkbox" name="chk_<?php echo $row['id']; ?>" />
						</span>
					</td>
					<td class="aligncenter" width="10">
						#<?php echo $row['id']; ?>
					</td>
					<td width="150"><?php echo $row['fullname']; ?></td>
					<td width="100"><?php echo $row['mobile']; ?></td>
					<td width="100"><?php echo $row['address']; ?></td>
					<td width="100">
						<?php echo $row['title']; ?>
					</td>
					<td width="100">
						<?php echo $row['requirement']; ?>
					</td>
					<td width="100"><?php echo $row['create_date']; ?></td>
					<td class="center" width="20">
						<?php echo anchor('admin/extension/deletecontact/' . $row['id'], '<span class="icon-trash"></span>', array('class' => 'deleterow', 'rel' => 'Liên hệ #' . $row['id'])) ?>
					</td>
				</tr>
				<?php echo form_hidden('ID[]', $row['id']); ?>
			<?php endforeach; ?>

        </table>
        <input type="hidden" name="action" class="action" value="" />
		<?php echo form_close(); ?>
		<?php
		$this->pagination->initialize($config);
		echo $this->pagination->create_links();
		?>

		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>