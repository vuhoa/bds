<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Feb 3, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <div class="pageicon"><span class="iconfa-sitemap"></span></div>
    <div class="pagetitle">
        <h1>Tên công ty</h1>
    </div>
</div><!--pageheader-->
<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Module Hiển thị Tên công ty</h4>
            <div class="widgetcontent">
				<p><label for="link">Name</label>
					<span class="field">
						<input type="text" name="content" value="<?php echo $detail['content']; ?>" id="content" class="input-block-level" />
					</span>
				</p>
                <p><label for="link">Email</label>
                    <span class="field">
						<input type="email" name="link_left" value="<?php echo $detail['link_left']; ?>" id="content" class="input-block-level" />
					</span>
                </p>

                <p><label for="link">Email Password</label>
                    <span class="field">
						<input type="password" name="link_right" value="<?php echo $detail['link_right']; ?>" id="content" class="input-block-level" />
					</span>
                </p>
                <p>
                    <span class="field">
                        <button type="submit" class="btn btn-submit btn-primary btn-rounded"><i class="iconfa-save"></i> Lưu</button>
                    </span>
                </p>
            </div><!--widgetcontent-->
        </div>
		<?php echo form_close(); ?>
		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>
