<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h5>Đơn hàng</h5>
        <h1>#<?php echo $order['id']; ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>
		<div class="widget">
            <h4 class="widgettitle">Thông tin Khách hàng</h4>
            <div class="widgetcontent">

                <p>
					<?php echo form_label('Khách hàng', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => $order['fullname'],
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => 'Chưa có tên khách hàng',
							'readonly' => true
						));
						?>
                    </span>
                </p>
				<p>
					<?php echo form_label('Địa chỉ', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => $order['address'],
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => 'Chưa có Địa chỉ',
							'readonly' => true
						));
						?>
                    </span>
                </p>
				<p>
					<?php echo form_label('Email', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => $order['email'],
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => 'Chưa có Email',
							'readonly' => true
						));
						?>
                    </span>
                </p>
				<p>
					<?php echo form_label('Điện thoại', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => $order['mobile'],
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => 'Chưa có số điện thoại',
							'readonly' => true
						));
						?>
                    </span>
                </p>
				<p>
					<?php echo form_label('Yêu cầu thêm của khách hàng', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => $order['requirement'],
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => 'Không có yêu cầu thêm',
							'readonly' => true
						));
						?>
                    </span>
                </p>
				<p>
					<?php echo form_label('Tổng giá trị đơn hàng', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => number_format($order['total'], 0, '', '.') . ' (đ)',
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => 'Giá trị đơn hàng cũ',
							'readonly' => true
						));
						?>
                    </span>
                </p>
				<p>
					<?php echo form_label('Ngày đặt hàng', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => $order['create_date'],
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => '',
							'readonly' => true
						));
						?>
                    </span>
                </p>
				<?php if ($order['status']) { ?>
					<p>
						<?php echo form_label('Ngày xử  lý đơn hàng', 'label') ?>
						<span class="field">
							<?php
							echo form_input(array(
								'name' => 'name',
								'value' => $order['update_date'],
								'id' => 'name',
								'class' => 'input-block-level',
								'placeholder' => '',
								'readonly' => true
							));
							?>
						</span>
					</p>
				<?php } ?>
				<p>
					<?php echo form_label('Trạng thái đơn ', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'value' => ($order['status']) ? 'Đã xử lý' : 'Chưa xử lý',
							'id' => 'name',
							'class' => 'input-block-level',
							'placeholder' => 'Object name',
							'readonly' => true
						));
						?>
                    </span>
                </p>
                <p>
                    <span class="field">
						<?php
						if (!$order['status'])
							echo anchor('admin/extension/processorderdetail/' . $order['id'], '<i class="iconfa-save"></i> Xử lý đơn hàng', array('class' => 'btn btn-primary btn-rounded'));
						?>
						<?php echo anchor('admin/extension/order', '<i class="iconfa-retweet"></i> Hủy', array('class' => 'btn btn-primary')); ?>
                    </span>

                </p>

            </div><!--widgetcontent-->
        </div>

		<?php
		if (!empty($products)) {
			$html = '<table class="table table-bordered table-responsive cart_summary">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th class="cart_product">Sản phẩm</th>';
			$html .= '<th>Chi tiết</th>';
			$html .= '<th>Tình trạng.</th>';
			$html .= '<th>Đơn giá</th>';
			$html .= '<th>Số lượng đặt mua</th>';
			$html .= '<th>Thành tiền</th>';
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			//
			$total = 0;
			foreach ($products as $product) {

				if ($product['stock']) {
					$price = 0;
				} else {
					$price = $orders->$product['id'] * $product['price'];
				}
				$total += $price;
				$html .= '<tr class="gradeX">';
				$html .= '<td width="60">';
				$html .= anchor('san-pham/' . $product['alias'], img(array('src' => $product['img'], 'width' => 60)), array('target' => '_blank'));
				$html .= '</td>';
				$html .= '<td class="cart_description">';
				$html .= '<p class="product-name">' . anchor('admin/record/edit/2/0/' . $product['id'], $product['name'], array('target' => '_blank')) . '</p>';
				$html .= '<small class="cart_ref">SKU : #' . $product['id'] . '</small><br>';
				$html .= '</td>';
				$html .= '<td class="cart_avail">';
				if ($product['stock'])
					$html .= '<span class="label label-unsuccess">Hết hàng</span>';
				else
					$html .= '<span class="label label-success">Còn hàng</span>';
				$html .= '</td>';
				$html .= '<td class="price"><span>' . number_format($product['price'], 0, '', '.') . ' đ</span></td>';
				$html .= '<td class="qty">';
				$html .= $orders->$product['id'];
				$html .= '</td>';
				$html .= '<td class="price">';
				$html .= '<span>' . number_format($price, 0, '', '.') . ' đ</span>';
				$html .= '</td>';
				$html .= '</tr>';
			}
			//
			$html .= '</tbody>';
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td colspan="2" rowspan="2"></td>';
			$html .= '<td colspan="3">Tổng sản phẩm (Đã bao gồm thuế)</td>';
			$html .= '<td colspan="2">' . number_format($total, 0, '', '.') . ' đ</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="3"><strong>Tổng tiền</strong></td>';
			$html .= '<td colspan="2"><strong>' . number_format($total, 0, '', '.') . ' đ</strong></td>';
			$html .= '</tr>';
			$html .= '</tfoot>  ';
			$html .= '</table>';
			echo $html;
		}else {
			echo "Đơn hàng đã cũ của hệ thống!";
		}
		?>

		<?php echo form_close(); ?>

		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>