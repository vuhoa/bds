<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Feb 3, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <div class="pageicon"><span class="iconfa-sitemap"></span></div>
    <div class="pagetitle">
        <h1>Quản lý Logo</h1>
    </div>
</div><!--pageheader-->
<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Module hiển thị logo</h4>
            <div class="widgetcontent">
				<p><label for="img">Logo</label>
					<span class="field">
						<input type="file" name="single_file" value="" class="clear single_file" f_type="14" field_id="37" rel="img">
						<span class="preView">
							<?php
							if ($detail['img']) {
								echo img(array('src' => 'images/'.$detail['img'], 'class' => 'thumbs'));
								?>
								<input type="hidden" name="img" value="<?php echo $detail['img']; ?>">
								<?php
							}
							?>
						</span>
						<a style="display:none;" class="remove_single iconsweets-trashcan" name="img" record="0" title="Xóa"></a>
						<span class="clear"></span>

					</span></p>
                <p class="html_type"></p>
                <p>
                    <span class="field">
                        <button type="submit" class="btn btn-submit btn-primary btn-rounded"><i class="iconfa-save"></i> Lưu</button>
                    </span>
                </p>
            </div><!--widgetcontent-->
        </div>
		<?php echo form_close(); ?>
		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>
