<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="Tìm kiếm..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h5>Tạo đối tượng Bảng</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open('', array('class' => 'objects')); ?>
        <?php echo anchor('admin/group/add', '<i class="iconfa-plus"></i> Thêm mới', array('class' => 'btn btn-primary ')); ?>
<!--        <button type="button" id="btn_update" class="btn btn-primary btn-rounded"><i class="iconfa-pencil"></i> Cập nhật</button>-->
        <button type="button" id="btn_delete" class="btn btn-danger btn-rounded"><i class="iconfa-remove"></i> Xóa</button>

        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">ID</th>
                <th class="head1">Tên nhóm</th>
                <th class="head0">Ngày tạo</th>
                <th class="head1">Trạng thái</th>
                <th class="head0">Xóa</th>
            </tr>

            <?php foreach ($groups as $group): ?>
                <tr class="gradeX">
                    <td class="aligncenter" width="10">
                        <span class="center">
                            <input type="checkbox" name="chk_<?php echo $group['id']; ?>" />
                        </span>
                    </td>
                    <td width="30"><?php echo $group['id']; ?></td>
                    <td><?php echo anchor('admin/group/edit/' . $group['id'], $group['name']); ?></td>
                    <td><?php echo $group['create_time']; ?></td>
                    <td class="center" width="30">
                        <?php
                        if ($group['status'])
                            echo anchor('admin/group/disable/' . $group['id'], '<span class="icon-ok"></span>');
                        else
                            echo anchor('admin/group/enable/' . $group['id'], '<span class="icon-remove"></span>');
                        ?>
                    </td>
                    <td class="center" width="20">
                        <?php echo anchor('admin/group/delete/' . $group['id'], '<span class="icon-trash"></span>', array('class' => 'deleterow')) ?>
                    </td>
                </tr>
                <?php echo form_hidden('ID[]', $group['id']); ?>
            <?php endforeach; ?>

        </table>
        <input type="hidden" name="action" class="action" value="" />
        <?php echo form_close(); ?>
        <?php
        $this->pagination->initialize($config);
        echo $this->pagination->create_links();
        ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>