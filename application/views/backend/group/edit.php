<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="Tìm kiếm..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h5>Sửa đối tượng Bảng</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Chỉnh sửa nhóm</h4>
            <div class="widgetcontent">
                <?php echo form_hidden('ID', $group['id']); ?>
                <p>
                    <?php echo form_label('Tên nhóm', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'name',
                            'id' => 'name',
                            'value' => $group['name'],
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Tên nhóm'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Ngày tạo', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'create_time',
                            'id' => 'create_time',
                            'value' => $group['create_time'],
                            'class' => 'input-block-level',
                            'READONLY' => true,
                            'placeholder' => 'Ngày tạo'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <span class="field">
                        <button type="text" class="btn btn-primary btn-rounded"><i class="iconfa-save"></i> Lưu</button>
<!--                        <button type="reset" class="btn btn-primary btn-rounded"><i class="iconfa-refresh"></i> Reset</button>-->
                        <?php echo anchor('admin/group', '<i class="iconfa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                    </span>

                </p>

            </div><!--widgetcontent-->
        </div>


        <?php echo form_close(); ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>