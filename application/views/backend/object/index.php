<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h5>Tạo đối tượng Table</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open('', array('class' => 'objects')); ?>
        <?php echo anchor('admin/object/add', '<i class="iconfa-plus"></i> Thêm mới', array('class' => 'btn btn-primary ')); ?>
        <button type="button" id="btn_update" class="btn btn-primary btn-rounded"><i class="iconfa-pencil"></i> Cập nhật</button>
        <button type="button" id="btn_delete" class="btn btn-danger btn-rounded"><i class="iconfa-remove"></i> Xóa</button>

        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">ID</th>
                <th class="head0">Ord</th>
                <th class="head1">Label</th>
                <th class="head0">Table</th>
                <th class="head1">Status</th>
                <th class="head0"></th>
            </tr>

            <?php foreach ($rows as $row): ?>
                <tr class="gradeX">
                    <td class="aligncenter" width="10">
                        <span class="center">
                            <input type="checkbox" name="chk_<?php echo $row['id']; ?>" />
                        </span>
                    </td>
                    <td width="30"><?php echo $row['id']; ?></td>
                    <td width="20">
                        <?php echo form_input('ord_' . $row['id'], $row['ord'], 'class="input-small"'); ?>
                    </td>
                    <td><?php echo anchor('admin/object/edit/' . $row['id'], $row['label']); ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td class="center" width="20">
                        <?php
                        if ($row['status'])
                            echo anchor('admin/object/disable/' . $row['id'], '<span class="icon-ok"></span>');
                        else
                            echo anchor('admin/object/enable/' . $row['id'], '<span class="icon-remove"></span>');
                        ?>
                    </td>
                    <td class="center" width="20">
                        <?php echo anchor('admin/object/delete/' . $row['id'], '<span class="icon-trash"></span>', array('class' => 'deleterow')) ?>
                    </td>
                </tr>
                <?php echo form_hidden('ID[]', $row['id']); ?>
            <?php endforeach; ?>

        </table>
        <input type="hidden" name="action" class="action" value="" />
        <?php echo form_close(); ?>
        <?php
        $this->pagination->initialize($config);
        echo $this->pagination->create_links();
        ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>