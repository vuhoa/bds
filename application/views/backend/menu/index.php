<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h5></h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open('', array('class' => 'menus')); ?>
		<?php echo anchor('admin/menu/add/' . $id, '<i class="iconfa-plus"></i> Thêm mới', array('class' => 'btn btn-primary ')); ?>
        <button type="button" id="btn_update" class="btn btn-primary btn-rounded"><i class="iconfa-pencil"></i> Cập nhật</button>
        <button type="button" id="btn_delete" class="btn btn-danger btn-rounded"><i class="iconfa-remove"></i> Xóa</button>

        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">ID</th>
                <th class="head0">Tên Menu</th>
				<th class="head1">Thứ tự</th>
                <th class="head0" width="40">Status</th>
                <th class="head1">#</th>
            </tr>

			<?php
			if ($menus) {
				foreach ($menus as $row):
					?>
					<tr class="gradeX">
						<td class="aligncenter" width="10">
							<span class="center">
								<input type="checkbox" name="chk_<?php echo $row['id']; ?>" />
							</span>
						</td>
						<td width="20"><?php echo $row['id']; ?></td>
						<td><?php echo anchor('admin/menu/edit/' . $row['id'], $row['name']); ?></td>
						<td width="20">
							<?php echo form_input('ord_' . $row['id'], $row['ord'], 'class="input-small"'); ?>
						</td>
						<td class="center" width="40">
							<?php
							if ($row['status'])
								echo anchor('admin/menu/disable/' . $row['id'], '<span class="icon-ok"></span>');
							else
								echo anchor('admin/menu/enable/' . $row['id'], '<span class="icon-remove"></span>');
							?>
						</td>
						<td class="center" width="20">
							<?php echo anchor('admin/menu/delete/' . $row['id'], '<span class="icon-trash"></span>', array('class' => 'deleterow')) ?>
						</td>
					</tr>
					<?php echo form_hidden('ID[]', $row['id']); ?>
					<?php
				endforeach;
			}
			?>

        </table>
        <input type="hidden" name="action" class="action" value="" />
		<?php echo form_close(); ?>

		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>