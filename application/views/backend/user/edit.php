<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="Tìm kiếm..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h5>Sửa đối tượng Bảng</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->
<?php $roleId = $this->session->userdata('role_id');?>
<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Chỉnh sửa tài khoản</h4>
            <div class="widgetcontent">
                <?php echo form_hidden('ID', $user['id']); ?>
                <p>
                    <?php echo form_label('Tên thành viên', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'fullname',
                            'id' => 'fullname',
                            'value' => $user['fullname'],
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Tên đầy đủ'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Ảnh đại diện', 'label') ?>
                    <span class="field">
                        <?php
                        echo $this->users->__htmlFile('avatar',$user['avatar']);
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Tài khoản', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'username',
                            'value' => $user['username'],
                            'id' => 'username',
                            'class' => 'input-block-level',
                            'READONLY' => true,
                            'placeholder' => 'Tài khoản',
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Mật khẩu', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_password(array(
                            'name' => 'password',
                            'id' => 'password',
                            'class' => 'input-block-level',
                            'value' => $user['password'],
                            'READONLY' => true,
                            'placeholder' => 'Mật khẩu'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Mật khẩu mới', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_password(array(
                            'name' => 'newpassword',
                            'id' => 'newpassword',
                            'class' => 'input-block-level',
                            'placeholder' => 'Mật khẩu'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Nhóm', 'label') ?>
                    <span class="field">
                        <?php
                        $roleArray = array('1' => 'Quản trị cấp cao','2' => 'Quản trị hệ thống','3' => 'Thành viên');
                        if ($roleId < 3)
                            echo form_dropdown('role_id',$roleArray , $user['role_id']);
                        else
                            echo form_dropdown('role_id',$roleArray , $user['role_id'],'disabled="disabled"');
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Email', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'email',
                            'value' => $user['email'],
                            'id' => 'email',
                            'class' => 'input-block-level',
                            'READONLY' => true,
                            'placeholder' => 'Email thành viên'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Số điện thoại', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'mobile',
                            'value' => $user['mobile'],
                            'id' => 'mobile',
                            'class' => 'input-block-level',
                            'placeholder' => 'Số điện thoại'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Địa chỉ', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'address',
                            'value' => $user['address'],
                            'id' => 'address',
                            'class' => 'input-block-level',
                            'placeholder' => 'Địa chỉ'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <span class="field">
                        <button type="text" class="btn btn-primary btn-rounded"><i class="iconfa-save"></i> Lưu</button>
<!--                        <button type="reset" class="btn btn-primary btn-rounded"><i class="iconfa-refresh"></i> Reset</button>-->
                        <?php echo anchor('admin/user', '<i class="iconfa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                    </span>

                </p>

            </div><!--widgetcontent-->
        </div>


        <?php echo form_close(); ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>