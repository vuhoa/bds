<?php
/**
 * Description of login
 * @author trungthuc
 * @date Jan 26, 2015
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?= $title ?></title>
		<?php echo link_tag('template/backend/css/style.default.css'); ?>
		<?php echo link_tag('template/backend/css/style.navyblue.css'); ?>

		<?php echo script_tag('template/backend/js/jquery-1.9.1.min.js'); ?>
		<?php echo script_tag('template/backend/js/jquery-migrate-1.1.1.min.js'); ?>
		<?php echo script_tag('template/backend/js/jquery-ui-1.9.2.min.js'); ?>
		<?php echo script_tag('template/backend/js/bootstrap.min.js'); ?>
		<?php echo script_tag('template/backend/js/custom.js'); ?>
        <script type="text/javascript">
			jQuery(document).ready(function () {
				jQuery('#login').submit(function () {
					var u = jQuery('#username').val();
					var p = jQuery('#password').val();
					if (u == '' && p == '') {
						jQuery('.login-alert').fadeIn();
						return false;
					}
				});
			});
        </script>
    </head>

    <body class="loginpage">

        <div class="loginpanel">
            <div class="loginpanelinner">
                <div class="logo animate0 bounceIn"><?php echo img('template/backend/images/zweb_logo.png'); ?></div>
				<?php echo form_open('admin/login', array('id' => 'login')); ?>
                <div class="inputwrapper login-alert">
                    <div class="alert alert-error">Sai tài khoản hoặc mật khẩu.</div>
                </div>
                <div class="inputwrapper animate1 bounceIn">
					<?php
					echo form_input(array(
						'name' => 'username',
						'id' => 'username',
						'placeholder' => 'Tài khoản'
					));
					?>
                </div>
                <div class="inputwrapper animate2 bounceIn">
					<?php
					echo form_input(array(
						'type' => 'password',
						'name' => 'password',
						'id' => 'password',
						'placeholder' => 'Mật khẩu'
					));
					?>
                </div>
                <div class="inputwrapper animate3 bounceIn">
                    <button name="submit">Đăng nhập</button>
                </div>
                <div class="inputwrapper animate4 bounceIn">
                    <label><input type="checkbox" class="remember" name="signin" /> Lưu mật khẩu</label>
                </div>

				<?php echo form_close(); ?>
            </div><!--loginpanelinner-->
        </div><!--loginpanel-->

        <div class="loginfooter">
            <p>&copy; <?php echo date('Y'); ?> Powered by <a href="http://zweb.vn" style="color: #FFF" target="_blank">Zweb</a>. All Rights Reserved.</p>
        </div>

    </body>
</html>
