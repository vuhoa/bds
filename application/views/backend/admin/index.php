<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>

<div class="pageheader">
    <div class="pageicon"><span class="iconfa-laptop"></span></div>
    <div class="pagetitle">
        <h5>Thông tin quản trị</h5>
        <h1>Bảng điều khiển</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <div class="row-fluid">
            <div id="dashboard-left" class="span8">

                <ul class="shortcuts">
                    <li class="events">
                        <a href="">
                            <span class="shortcuts-icon iconsi-event"></span>
                            <span class="shortcuts-label">Events</span>
                        </a>
                    </li>
                    <li class="products">
                        <a href="">
                            <span class="shortcuts-icon iconsi-cart"></span>
                            <span class="shortcuts-label">Products</span>
                        </a>
                    </li>
                    <li class="archive">
                        <a href="">
                            <span class="shortcuts-icon iconsi-archive"></span>
                            <span class="shortcuts-label">Archives</span>
                        </a>
                    </li>
                    <li class="help">
                        <a href="">
                            <span class="shortcuts-icon iconsi-help"></span>
                            <span class="shortcuts-label">Help</span>
                        </a>
                    </li>
                    <li class="last images">
                        <a href="">
                            <span class="shortcuts-icon iconsi-images"></span>
                            <span class="shortcuts-label">Images</span>
                        </a>
                    </li>
                </ul>

                <br />


            </div><!--span8-->

        </div><!--row-fluid-->


    </div><!--maincontentinner-->
</div><!--maincontent-->
<?php $this->load->view('backend/footer'); ?>