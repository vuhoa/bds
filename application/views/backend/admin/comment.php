<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
$position = 0;
$stt = 1;
?>
<?php $this->load->view('backend/breadcrumbs'); ?>

<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="iconfa-laptop"></span></div>
    <div class="pagetitle">
        <h5>All Features Summary</h5>
        <h1>Quản lý bình luận</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
    <table class="table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Comment ID</th>
                            <th>Họ tên</th>
                            <th>Thư điện tử</th>
                            <th>Địa chỉ ip</th>
                            <th>Nội dung</th>
                            <th>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php 
                     foreach($comment as $item){?>
                     
                        <tr>
                            <td><?php echo $stt ?></td>
                            <td><?php echo $item['cmt_id'] ?></td>
                            <td><?php echo $item['name'] ?></td>
                            <td><?php echo $item['email'] ?></td>
                            <td><?php echo $item['cmt_ip'] ?></td>
                            <td><?php echo $item['cmt_content'] ?></td>
                            <td><?php echo anchor('admin/admin/deletecomment/' . $item['cmt_id'], '<span class="icon-trash"></span>', array('class' => 'deleterow')) ?></td>
                            <!-- Modal -->
                            <div id="myModal_<?php echo $position ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">
                              </div>
                            </div>
                        </tr> 
                        <?php $position++; $stt++; }?> 
                    </tbody>
                </table> 
		<?php $this->load->view('backend/footer'); ?>

    </div><!--maincontentinner-->
</div><!--maincontent-->


<?php $this->load->view('backend/footer'); ?>

</div><!--maincontentinner-->
</div><!--maincontent-->