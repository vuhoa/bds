<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
$position = 0;
$stt = 1;
?>
<?php $this->load->view('backend/breadcrumbs'); ?>

<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="iconfa-laptop"></span></div>
    <div class="pagetitle">
        <h5>All Features Summary</h5>
        <h1>Đơn đặt hàng</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
    <table class="table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Hóa đơn ID</th>
                            <th>Tên người mua</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Yêu cầu</th>
                            <th>Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php 
                     foreach($order as $item){?>
                     
                        <tr>
                            <td><?php echo $stt ?></td>
                            <td><?php echo $item['id'] ?></td>
                            <td><?php echo $item['name'] ?></td>
                            <td><?php echo $item['address'] ?></td>
                            <td><?php echo $item['phone'] ?></td>
                            <td><?php echo $item['email'] ?></td>
                            <td><?php echo $item['mgs'] ?></td>
                            <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal_<?php echo $position ?>">View</button></td>
                            <!-- Modal -->
                            <div id="myModal_<?php echo $position ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Chi tiết đơn hàng - HD<?php echo $item['id'] ?> - <?php echo $item['name'] ?></h4>
                                  </div>
                                  <div class="modal-body">
                                  <?php $order_product=$this->adm->__order_product($item['id']) ?>
                                  <div class="row" style="margin-left:0px;">
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left">Sản phẩm ID</div>
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left">Tên sản phẩm</div>
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left">Số lượng</div>
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left">Tổng giá trị</div>
                                     
                                     </div>
                                    </div>
                                  <?php   
                                    foreach($order_product as $items){?>
                                    
                                    <div class="row" style="margin-left: 10px !important; ">
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left"><?php echo $items['product_id'] ?></div>
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left"><?php echo $items['name'] ?></div>
                                  
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left"><?php echo $items['qty'] ?></div>
                                     <div class="col-sm-3 col-md-3 col-lg-3" style="width:25%; float:left"><?php echo number_format($items['values'])?></div>
                                     
                                    </div>
                                 <?php }?> 
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                                  </div>
                                </div>

                              </div>
                            </div>
                        </tr> 
                        <?php $position++; $stt++; }?> 
                    </tbody>
                </table> 
		<?php $this->load->view('backend/footer'); ?>

    </div><!--maincontentinner-->
</div><!--maincontent-->


<?php $this->load->view('backend/footer'); ?>

</div><!--maincontentinner-->
</div><!--maincontent-->