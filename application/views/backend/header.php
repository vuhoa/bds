<?php
/**
 * Description of profile
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<div class="header">
    <div class="logo">
		<?php echo anchor('admin', img('template/backend/images/z_logo.png')) ?>
    </div>
    <div class="headerinner">
        <ul class="headmenu">
            <li class="odd">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="count">0</span>
                    <span class="head-icon head-message"></span>
                    <span class="headmenu-label">Tin nhắn</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-header">Tin nhắn</li>
                    <li><a href=""><span class="icon-envelope"></span> New message from <strong>Jack</strong> <small class="muted"> - 19 hours ago</small></a></li>
                    <li class="viewmore"><a href="messages.html">View More Messages</a></li>
                </ul>
            </li>
            <li>
                <a class="dropdown-toggle" data-toggle="dropdown" data-target="#">
                    <span class="count">0</span>
                    <span class="head-icon head-users"></span>
                    <span class="headmenu-label">Người dùng</span>
                </a>
                <ul class="dropdown-menu newusers">
                    <li class="nav-header">New Users</li>
                    <li>
                        <a href="">
                            <img src="images/photos/thumb1.png" alt="" class="userthumb" />
                            <strong>Draniem Daamul</strong>
                            <small>April 20, 2013</small>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="odd">
                <a class="dropdown-toggle" data-toggle="dropdown" data-target="#">
                    <span class="count">1</span>
                    <span class="head-icon head-bar"></span>
                    <span class="headmenu-label">Thống kê</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-header">Thống kê</li>
                    <li><a href=""><span class="icon-align-left"></span> New Reports from <strong>Products</strong> <small class="muted"> - 19 hours ago</small></a></li>
                    <li class="viewmore"><a href="charts.html">View More Statistics</a></li>
                </ul>
            </li>
            <li class="right">
                <div class="userloggedinfo">
                    <img src="images/photos/thumb1.png" alt="" />
                    <div class="userinfo">
                        <h5><?php echo $this->session->userdata('fullname'); ?> <small>- <?php echo $this->session->userdata('email'); ?></small></h5>
                        <ul>
                            <li><?php echo anchor('admin/user/profile', 'Thông tin cá nhân') ?></li>
                            <li><?php echo anchor('admin/user/setting', 'Cài đặt tài khoản') ?></li>
                            <li><?php echo anchor('admin/logout', 'Thoát') ?></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul><!--headmenu-->
    </div>
</div>