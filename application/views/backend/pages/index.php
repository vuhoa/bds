<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h5></h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open('', array('class' => 'menus')); ?>

        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">ID</th>
                <th class="head0">Tên Pages</th>
				<th>Page</th>
                <th class="head1">#</th>
            </tr>

			<?php
			if ($pages) {
				foreach ($pages as $row):
					?>
					<tr class="gradeX">
						<td class="aligncenter" width="10">
							<span class="center">
								<input type="checkbox" name="chk_<?php echo $row['id']; ?>" />
							</span>
						</td>
						<td width="20"><?php echo $row['id']; ?></td>
						<td><?php echo anchor('admin/pages/edit/' . $row['id'], $row['label']); ?></td>
						<td><?php echo $row['name'] ?></td>
						<td class="center" width="20">
							#
						</td>
					</tr>
					<?php echo form_hidden('ID[]', $row['id']); ?>
					<?php
				endforeach;
			}
			?>

        </table>
        <input type="hidden" name="action" class="action" value="" />
		<?php echo form_close(); ?>

		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>