<?php

/**
 * Description of object
 * @author trungthuc
 * @date Jan 29, 2015
 */
switch ($type):
    case 1:
        echo form_label('Độ dài', 'length');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'length',
            'class' => 'input-small',
            'placeholder' => 'Length')
        );
        echo '</span>';
        break;
    case 2:
    case 3:
        break;
    case 4:
        echo form_label('Width ', 'width');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'width',
            'class' => 'input-small',
            'placeholder' => 'Width')
        );
        echo '</span>';
        echo form_label('Height ', 'height');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'height',
            'class' => 'input-small',
            'placeholder' => 'Height')
        );
        echo '</span>';
        echo form_label('Editor ', 'height');
        echo '<span class="field">';
        echo form_radio(array(
            'name' => 'editor',
            'value' => 0,
            'checked' => FALSE,
            'style' => 'margin:10px',
        )) . ' Editor';
        echo form_radio(array(
            'name' => 'editor',
            'value' => 1,
            'checked' => FALSE,
            'style' => 'margin:10px',
        )) . ' WYSIWYG';
        echo form_radio(array(
            'name' => 'editor',
            'value' => 2,
            'checked' => TRUE,
            'style' => 'margin:10px',
        )) . ' FCK';
        echo '</span>';
        break;
    case 5:
    case 6:
    case 7:
        break;
    case 23:
    case 8:
        echo form_label('Danh mục', 'parent_id');
        echo '<span class="field">';
        echo form_dropdown('parent_id', $data, '', 'class="uniformselect"');
        echo '</span>';
        break;
    case 9:
        echo form_label('Mô tả', 'descriptions');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'descriptions',
            'class' => 'input-small span5',
            'placeholder' => 'Mô tả cho checkbox')
        );
        echo '</span>';
        break;
    case 10:
        echo form_label('Đối tượng chọn Ratio', 'parent_id');
        echo '<span class="field">';
        echo form_dropdown('parent_id', $data, '', 'class="uniformselect"');
        echo '</span>';
        break;
    case 11:
    case 12:
    case 13:
    case 14:
            echo form_label('Tạo ảnh thumb ', 'process');
            echo '<span class="field">';
            echo form_radio(array(
                'name' => 'process',
                'value' => 1,
                'checked' => FALSE,
                'style' => 'margin:10px',
                'class' => 'image_process'
            )) . ' Crop ảnh';
            echo form_radio(array(
                'name' => 'process',
                'value' => 2,
                'checked' => FALSE,
                'style' => 'margin:10px',
                'class' => 'image_process'
            )) . ' Resize ảnh';
            echo '</span>';
            echo '</p>';
            echo '<p class="image_type">';
        break;
    case 15:
        //Kiem tra xem object da co truong upload anh goc chua
        $check = $this->objects->__checkFieldImageObject($object_id);
        if ($check) {
            echo form_label('Tùy chọn tạo thumb ', 'process');
            echo '<span class="field">';
            echo form_radio(array(
                'name' => 'process',
                'value' => 1,
                'checked' => FALSE,
                'style' => 'margin:10px',
                'class' => 'image_process'
            )) . ' Crop ảnh';
            echo form_radio(array(
                'name' => 'process',
                'value' => 2,
                'checked' => FALSE,
                'style' => 'margin:10px',
                'class' => 'image_process'
            )) . ' Resize ảnh';
            echo '</span>';
            echo '</p>';
            echo '<p class="image_type">';
        } else {
            echo '<span class="field"><div class="alert">';
            echo '<button data-dismiss="alert" class="close" type="button">×</button>';
            echo '<strong>Warning!</strong> Bạn chưa tạo trường upload IMAGE! Hãy tạo trường ảnh gốc trước khi tạo thumb.</div></span>';
        }
        break;
    case 17:
        echo form_label('Thêm tùy chọn xử lý ảnh ', 'process');
        echo '<span class="field">';
        echo form_radio(array(
            'name' => 'process',
            'value' => 0,
            'checked' => TRUE,
            'style' => 'margin:10px',
            'class' => 'image_process'
        )) . ' Giữ nguyên ảnh gốc';

        echo form_radio(array(
            'name' => 'process',
            'value' => 1,
            'checked' => FALSE,
            'style' => 'margin:10px',
            'class' => 'image_process'
        )) . ' Crop ảnh';
        echo form_radio(array(
            'name' => 'process',
            'value' => 2,
            'checked' => FALSE,
            'style' => 'margin:10px',
            'class' => 'image_process'
        )) . ' Resize ảnh';
        echo '</span>';
        echo '</p>';
        echo '<p class="image_type">';
        break;
    case 16:
    case 19:
    case 22:
        break;
    case 20:
        echo form_label('Lựa chọn từ ', 'parent_id');
        echo '<span class="field">';
        echo form_dropdown('parent_id', $data, '', 'class="uniformselect"');
        echo '</span>';
        break;
    case 21:
        echo form_label('Comment từ ', 'parent_id');
        echo '<span class="field">';
        echo form_dropdown('parent_id', $data, '', 'class="uniformselect"');
        echo '</span>';
        break;
    case 18:
        //Kiem tra xem object da co truong upload anh goc chua
        $check = $this->objects->__checkFieldMultiImageObject($object_id);
        if ($check) {
            echo form_label('Tùy chọn tạo thumb ', 'process');
            echo '<span class="field">';
            echo form_radio(array(
                'name' => 'process',
                'value' => 1,
                'checked' => FALSE,
                'style' => 'margin:10px',
                'class' => 'image_process'
            )) . ' Crop ảnh';
            echo form_radio(array(
                'name' => 'process',
                'value' => 2,
                'checked' => FALSE,
                'style' => 'margin:10px',
                'class' => 'image_process'
            )) . ' Resize ảnh';
            echo '</span>';
            echo '</p>';
            echo '<p class="image_type">';
        } else {
            echo '<span class="field"><div class="alert">';
            echo '<button data-dismiss="alert" class="close" type="button">×</button>';
            echo '<strong>Warning!</strong> Bạn chưa tạo trường Upload Multi ảnh gốc! Hãy tạo trường ảnh gốc trước khi tạo thumb.</div></span>';
        }
        break;
    default:
        break;
endswitch;
?>
