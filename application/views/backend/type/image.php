<?php

/**
 * Description of media
 * @author trungthuc
 * @date Jan 30, 2015
 */
switch ($type) {
    case 14:
        echo form_label('Width ', 'width');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'width',
            'class' => 'input-small',
            'placeholder' => 'Width')
        );
        echo '</span>';

        echo form_label('Height ', 'height');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'height',
            'class' => 'input-small',
            'placeholder' => 'Height')
        );
        echo '</span>';
        break;
    case 17:
        if ($type_select != 0):
            echo form_label('Width ', 'width');
            echo '<span class="field">';
            echo form_input(array(
                'name' => 'width',
                'class' => 'input-small',
                'placeholder' => 'Width')
            );
            echo '</span>';

            echo form_label('Height ', 'height');
            echo '<span class="field">';
            echo form_input(array(
                'name' => 'height',
                'class' => 'input-small',
                'placeholder' => 'Height')
            );
            echo '</span>';
        endif;
        break;
    case 15:
        //Thumb
        //Display Parent image
        echo form_label('Ảnh gốc ', 'parent_id');
        echo '<span class="field">';
        echo form_dropdown('parent_id', $data, '', 'class="uniformselect"');
        echo '</span>';
        echo form_label('Width ', 'width');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'width',
            'class' => 'input-small',
            'placeholder' => 'Width')
        );
        echo '</span>';

        echo form_label('Height ', 'height');
        echo '<span class="field">';
        echo form_input(array(
            'name' => 'height',
            'class' => 'input-small',
            'placeholder' => 'Height')
        );
        echo '</span>';
        break;
    default:
        break;
}
