function convert(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
    str = str.replace(/-+-/g, "-");
    str = str.replace(/^\-+|\-+$/g, "");
    str = str.replace(/–/g, "");
    return str;
}

jQuery.noConflict();

jQuery(document).ready(function () {

    // dropdown in leftmenu
    jQuery('.leftmenu .dropdown > a').click(function () {
        if (!jQuery(this).next().is(':visible'))
            jQuery(this).next().slideDown('fast');
        else
            jQuery(this).next().slideUp('fast');
        return false;
    });

//    if (jQuery.uniform)
//        jQuery('input:checkbox, input:radio, .uniform-file').uniform();

    if (jQuery('.widgettitle .close').length > 0) {
        jQuery('.widgettitle .close').click(function () {
            jQuery(this).parents('.widgetbox').fadeOut(function () {
                jQuery(this).remove();
            });
        });
    }

    // add menu bar for phones and tablet
    jQuery('<div class="topbar"><a class="barmenu">' +
            '</a></div>').insertBefore('.mainwrapper');

    jQuery('.topbar .barmenu').click(function () {

        var lwidth = '260px';
        if (jQuery(window).width() < 340) {
            lwidth = '240px';
        }

        if (!jQuery(this).hasClass('open')) {
            jQuery('.rightpanel, .headerinner, .topbar').css({marginLeft: lwidth}, 'fast');
            jQuery('.logo, .leftpanel').css({marginLeft: 0}, 'fast');
            jQuery(this).addClass('open');
        } else {
            jQuery('.rightpanel, .headerinner, .topbar').css({marginLeft: 0}, 'fast');
            jQuery('.logo, .leftpanel').css({marginLeft: '-' + lwidth}, 'fast');
            jQuery(this).removeClass('open');
        }
    });

    // show/hide left menu
    jQuery(window).resize(function () {
        if (!jQuery('.topbar').is(':visible')) {
            jQuery('.rightpanel, .headerinner').css({marginLeft: '260px'});
            jQuery('.logo, .leftpanel').css({marginLeft: 0});
        } else {
            jQuery('.rightpanel, .headerinner').css({marginLeft: 0});
            jQuery('.logo, .leftpanel').css({marginLeft: '-260px'});
        }
    });

    // dropdown menu for profile image
    jQuery('.userloggedinfo img').click(function () {
        if (jQuery(window).width() < 480) {
            var dm = jQuery('.userloggedinfo .userinfo');
            if (dm.is(':visible')) {
                dm.hide();
            } else {
                dm.show();
            }
        }
    });

    /*
     * Trung Thuc
     * 29/01/2015
     */
    // expand/collapse boxes
    if (jQuery('.minimize').length > 0) {

        jQuery('.minimize').click(function () {
            if (!jQuery(this).hasClass('collapsed')) {
                jQuery(this).addClass('collapsed');
                jQuery(this).html("&#43;");
                jQuery(this).parents('.widgetbox')
                        .css({marginBottom: '20px'})
                        .find('.widgetcontent')
                        .hide();
            } else {
                jQuery(this).removeClass('collapsed');
                jQuery(this).html("&#8211;");
                jQuery(this).parents('.widgetbox')
                        .css({marginBottom: '0'})
                        .find('.widgetcontent')
                        .show();
            }
            return false;
        });

    }

    //Delete
    jQuery('.deleterow').click(function () {
        var rel = jQuery(this).attr('rel');
        var conf = confirm('Bạn có thực sự muốn xóa bản ghi ID=' + rel + ' ?');
        var link = jQuery(this).attr('href');
        if (conf)
            window.location = link;
        return false;
    });

    // check all checkboxes in table
    jQuery('.checkall').click(function () {
        var parentTable = jQuery(this).parents('table');
        var ch = parentTable.find('input[type=checkbox]');
        if (jQuery(this).is(':checked')) {
            //check all rows in table
            ch.each(function () {
                jQuery(this).attr('checked', true);
                jQuery(this).parent().addClass('checked');	//used for the custom checkbox style
                jQuery(this).parents('tr').addClass('selected'); // to highlight row as selected
            });
        } else {
            //uncheck all rows in table
            ch.each(function () {
                jQuery(this).attr('checked', false);
                jQuery(this).parent().removeClass('checked');	//used for the custom checkbox style
                jQuery(this).parents('tr').removeClass('selected');
            });
        }
    });

    //Update order
    jQuery('#btn_update').click(function () {
        jQuery('.action').val('update');
        jQuery('form.objects').submit();
    });

    jQuery('#btn_delete').click(function () {
        var checked = jQuery('input[type=checkbox]:checked').length;
        if (checked > 0) {
            var conf = confirm('Bạn có thực sự muốn xóa ' + checked + ' bản ghi đã chọn không?');
            if (conf) {
                jQuery('.action').val('delete');
                jQuery('form.objects').submit();
            }
            return false;
        } else {
            alert('Bạn hãy chọn bản ghi cần xóa!');
        }

    });

    //Ajax 
    jQuery('#data_type').change(function () {
        var type = jQuery('#data_type option:selected').val();

        jQuery.ajax({
            type: 'post',
            url: base_url + 'admin/type/object',
            data: {
                type: type,
                object_id: object_id,
            },
            dataType: 'html',
            success: function (html) {
                jQuery('.html_type').html(html);
            },
            complete: function () {
                //$('#btn-folder-desc').show();
                //$('#folder-description-editor').hide();
            }
        });

    });
    //Ajax media
    jQuery('.image_process').live('click', function () {
        var type = jQuery('#data_type option:selected').val();
        var type_select = jQuery(this).val();
        jQuery.ajax({
            type: 'post',
            url: base_url + 'admin/type/image',
            data: {
                type: type,
                type_select: type_select,
                object_id: object_id,
            },
            dataType: 'html',
            success: function (html) {
                jQuery('.image_type').html(html);
            },
            complete: function () {

            }
        });
    });
    //Multi upload
    var dem = 0;
    jQuery('a.add_file').click(function () {
        var name = jQuery(this).attr('name');
        var field_id = jQuery(this).attr('id');
        dem++;
        var html = '';
        html += '<p id="' + name + dem + '">';
        html += '<span class="field">';
        html += '<span class="imagePrev"></span>';
        html += '<input type="file" name="file" class="clear multiple" field_id="' + field_id + '" rel="' + name + '[]" />';
        html += '<input type="text" name="' + name + '_txt[]" class="description_file" placeholder="Mô tả..." />';
        html += '<a class="iconsweets-refresh3 change" title="Thay đổi"></a>';
        html += '<a class="remove iconsweets-trashcan" id="' + dem + '" name="' + name + '" vfile="" recordfd="0" title="Xóa"></a>';
        html += '<span class="clear"></span>';
        html += '</span></p>';
        jQuery(html).insertBefore('.add-' + name);
    });
    //Remove
    jQuery('.remove').live('click', function () {
        var name = jQuery(this).attr('name');
        name += jQuery(this).attr('id');
        var record_id = jQuery(this).attr('recordid');
        var vfile = jQuery(this).attr('vfile');
        //Delete tmp file
        jQuery.ajax({
            url: base_url + 'admin/type/delete',
            dataType: 'html',
            data: {
                record_id: record_id,
                vfile: vfile
            },
            type: 'post',
            success: function (xhr) {

            },
            complete: function () {
                jQuery('#' + name).remove();
            }
        });
    });
    //Change Image
    jQuery('.change').live('click', function () {
        var parentDiv = jQuery(this).parents('span[class="field"]');
        var imagePrev = parentDiv.find('span[class="imagePrev"]');
        var multiple = parentDiv.find('.multiple');
        jQuery(imagePrev).empty();
        jQuery(multiple).show();

    });
    //Upload image
    jQuery('input.multiple').live('change', function () {
        var el = jQuery(this);
        var name = jQuery(this).attr('rel');
        var parentDiv = jQuery(this).parents('span[class="field"]');
        var file_id = parentDiv.find('input[name="' + name + '"]');
        var imagePrev = parentDiv.find('span[class="imagePrev"]');
        var field_id = jQuery(this).attr('field_id');
        var tmp_id = parentDiv.find('.remove');
        var file_data = jQuery(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('field_id', field_id);
        if (file_id.length > 0) {
            form_data.append('file_id', jQuery(file_id).val());
        }

        jQuery.ajax({
            url: base_url + 'admin/type/upload',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (xhr) {
                imagePrev.html(xhr.html);
                if (file_id.length == 0) {
                    tmp_id.attr('vFile', xhr.value);
                    input_id = '<input type="hidden" name="' + name + '" value="' + xhr.value + '" />';
                    jQuery(input_id).insertBefore(imagePrev);
                }

            },
            complete: function () {
                //el.remove();
                el.hide();
            }
        });
    });

    /*
     * @Upload  single image/file
     */

    jQuery('.single_file').live('change', function () {
        var el = jQuery(this);
        var rel = jQuery(this).attr('rel');
        var type = jQuery(this).attr('f_type');
        var parentDiv = jQuery(this).parents('span[class="field"]');
        var preView = parentDiv.find('span[class="preView"]');
        var field_id = jQuery(this).attr('field_id');
        var remove = parentDiv.find('.remove_single');
        var file_data = jQuery(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('type', type);
        form_data.append('field_id', field_id);
        jQuery.ajax({
            url: base_url + 'admin/type/upload_single',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (xhr) {
                preView.html(xhr.html);
                if (xhr.file_name != null) {
                    file_input = '<input type="hidden" name="' + rel + '" value="' + xhr.file_name + '" />';
                    jQuery(file_input).insertBefore(el);
                }
//				if (xhr.arr_thumb != null) {
//					input = '';
//					jQuery.each(xhr.arr_thumb, function (key, value) {
//						input += '<input type="hidden" name="' + key + '" value="' + value + '" />';
//					});
//					jQuery(input).insertBefore(el);
//				}
            },
            complete: function () {
                el.hide();
                remove.show();
            }
        });
    });
    /*
     * Remove input
     * Hide element remove single.
     */
    jQuery('.remove_single').live('click', function () {
        var record = jQuery(this).attr('record');
        var object_id = jQuery('input[name="oID"]').val();
        var parentDiv = jQuery(this).parents('span[class="field"]');
        var single_file = parentDiv.find('.single_file');
        var preView = parentDiv.find('span[class="preView"]');
        var hidden = parentDiv.find('input[type="hidden"]');
        hidden.each(function () {
            var el = jQuery(this);
            var value = jQuery(this).val();
            var name = jQuery(this).attr('name');
            //Delete tmp file
            jQuery.ajax({
                url: base_url + 'admin/type/delete_single',
                dataType: 'html',
                data: {
                    name: name,
                    file: value,
                    record: record,
                    object: object_id
                },
                type: 'post',
                success: function (xhr) {

                },
                complete: function () {
                    el.remove();
                }
            });
            jQuery(this).remove();
        });
        preView.empty();
        jQuery(this).hide();
        single_file.show();
    });
    //Alias
    jQuery(".generator").click(function () {
        var value = jQuery("input[name=name]").val();
        var string = convert(value);
        var alias = jQuery("input[id=alias]");
        var object_id = jQuery('input[name="oID"]').val();
        jQuery.ajax({
            url: base_url + 'admin/type/auto_generator',
            dataType: 'json',
            data: {
                string: string,
                object: object_id
            },
            type: 'post',
            success: function (xhr) {
                alias.val(xhr);
            },
            complete: function () {

            }
        });
    }).keyup();

    //Select language
    jQuery('.selectLanguage').change(function () {
        var name = jQuery(this).val();
        window.location.href = base_url + 'admin/lang/update/' + name;
    });

    //Select Menu
    //type
    jQuery('#menu_type').change(function () {
        var type = jQuery('#menu_type option:selected').val();

        jQuery.ajax({
            type: 'post',
            url: base_url + 'admin/type/menu',
            data: {
                type: type
            },
            dataType: 'json',
            success: function (d) {
                switch (parseInt(d.type)) {
                    case 1:
                        jQuery('.html_menu').html('');
                        jQuery('.menu_url').html('');
                        jQuery('.html_menu_category').html('');
                        break;

                    case 2:
                        jQuery('.html_menu').html('');
                        var html = '';
                        html += '<label for="label">Pages</label>';
                        html += '<span class="field"><select name="row_id" id="menu_page">';
                        html += '<option>Chọn 1 page</option>';
                        for (var i = 0; i < d.data.length; i++) {
                            html += '<option value="' + d.data[i].id + '" data-name="' + d.data[i].name + '">' + d.data[i].label + '</option>';
                        }
                        html += '</select></span>';
                        jQuery('.html_menu').html(html);
                        jQuery('p.alias').show();
                        jQuery('.menu_url').html('');
                        jQuery('.html_menu_category').html('');
                        break;

                    case 3:
                        jQuery('p.alias').hide();
                        jQuery('.html_menu').html('');
                        var html = '';
                        html += '<p class="menu_url"><label for="label">URL</label>';
                        html += '<span class="field">';
                        html += '<input type="text" name="url" id="url" class="input-block-level" placeholder="URL">';
                        html += '</span></p>';
                        jQuery('p.name').append(html);

                        break;

                    case 4:
                        var html = '';
                        html += '<label for="label">Danh mục</label>';
                        html += '<span class="field"><select name="object_id" id="menu_category">';
                        html += '<option>Chọn 1 danh mục</option>';
                        for (var i = 0; i < d.data.length; i++) {
                            html += '<option value="' + d.data[i].id + '">' + d.data[i].label + '</option>';
                        }
                        html += '</select></span>';
                        jQuery('.html_menu').html(html);
                        jQuery('p.alias').show();
                        jQuery('.menu_url').html('');
                        break;

                    case 5:
                        var html = '';
                        html += '<label for="label">Bài viết</label>';
                        html += '<span class="field"><select name="object_id" id="menu_category">';
                        html += '<option>Chọn 1 danh mục</option>';
                        for (var i = 0; i < d.data.length; i++) {
                            html += '<option value="' + d.data[i].id + '">' + d.data[i].label + '</option>';
                        }
                        html += '</select></span>';
                        jQuery('.html_menu').html(html);
                        jQuery('p.alias').show();
                        jQuery('.menu_url').html('');
                        break;

                    default:
                        jQuery('.html_menu').html('');
                        jQuery('.html_menu_category').html('');
                        jQuery('.menu_url').html('');
                        break;
                }
                jQuery('.html_menu_single').html('');

            },
            complete: function () {

            }
        });

    });


    jQuery('#menu_category').live('change', function () {

        var objectId = jQuery('#menu_category option:selected').val();

        jQuery.ajax({
            type: 'post',
            url: base_url + 'admin/type/menu_object',
            data: {
                object_id: objectId
            },
            dataType: 'json',
            success: function (d) {
                var html = '';
                html += '<label for="label">Chọn tên</label>';
                html += '<span class="field"><select name="row_id" id="row_id" required=1>';
                html += '<option>Chọn 1 danh mục</option>';
                for (var i = 0; i < d.data.length; i++) {
                    html += '<option value="' + d.data[i].id + '">' + d.data[i].name + '</option>';
                }
                html += '</select></span>';

                jQuery('.html_menu_category').html(html);
                jQuery('.html_menu_category').append('<input name="object_name" type="hidden" value="' + d.object.name + '">');
                jQuery('.html_menu_category').append('<input name="row_alias" type="hidden" value="' + d.object.alias + '">');

            },
            complete: function () {

            }
        });

    });

    //select 1 row
    jQuery('#row_id').live('change', function () {
        var name = jQuery('#row_id option:selected').text();
        jQuery('input[name=name]').val(name);
        jQuery('input[name=alias]').val(convert(name));
    });

    //single post
    jQuery('#menu_page').live('change', function () {

        var pageName = jQuery('#menu_page option:selected').attr('data-name');
        var pageLabel = jQuery('#menu_page option:selected').text();
        jQuery('input[name=name]').val(pageLabel);
        jQuery('input[name=alias]').val(convert(pageLabel));
        jQuery('.html_menu').append('<input name="object_name" type="hidden" value="' + pageName + '">');

    });


});
