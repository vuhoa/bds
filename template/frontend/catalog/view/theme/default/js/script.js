$(document).ready(function() {
    var owl = $('.owl-carousel-slide');
    owl.owlCarousel({
        margin: 10,
        items:1,
        loop: false,
        autoplay: false,
        dotsEach:false
    })
})