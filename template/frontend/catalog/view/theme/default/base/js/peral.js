$(document).ready(function() {
    var owl = $('.owl-news').owlCarousel({
        items:1,
        margin:30,
        responsiveClass: true,
        autoplay:false,
        autoplayTimeout:5000,
        dots:false
    });

    $('.dots .item').click(function () {
        let number = $(this).attr('data-number');
        console.log(number -1);
        owl.trigger('to.owl.carousel', number - 1)
    });

    owl.on('changed.owl.carousel', function(event) {
        let currentItem = event.item.index + 1;
        $('.dots .item').removeClass('active');
        $('.dots .item-'+currentItem).addClass('active');
    });

    $('#year .item').click(function () {
        owl.trigger('to.owl.carousel', 0)
        let number = $(this).attr('data-number');
        $('#year .item').removeClass('active');
        $('#year .item-'+number).addClass('active');
        $('.year-item').removeClass('active');
        $('.year-item-'+number).addClass('active');
    });
})