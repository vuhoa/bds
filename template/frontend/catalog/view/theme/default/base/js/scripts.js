$(document).ready(function() {
    if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}
$('#owl-news').owlCarousel({
    items:1,
    margin:30,
    responsiveClass: true,
    autoplay:true,
    autoplayTimeout:5000,

  });
$('#owl-event').owlCarousel({
    // animateIn: 'fadeIn',
    items:1,
    // margin:30,
    responsiveClass: true,
    autoplay:true,
    autoplayTimeout:5000,

  });

    $('#owl-partner').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false,
                margin: 20
            }
        }
    });
    $('#owl-event .owl-prev span').html('<img src="images/pre.svg" height="14px" width="14px"/>');
    $('#owl-event .owl-next span').html('<img src="images/next.svg" height="14px" width="14px"/>');
})